#!/bin/bash
#PBS -N gpu_training_debug
#PBS -q gpu
#PBS -l select=1:ngpus=1:ncpus=8:mem=16gb:scratch_local=20gb:gpu_mem=16gb:gpu_cap=cuda60
#PBS -l walltime=16:00:00
#PBS -m ae

PROJECT_PATH=/storage/brno2/home/xdvora3o/KNN/project-image-colorizer

INDIR=$PROJECT_PATH # IMPORTANT: Change path to the project location
OUTDIR=$PROJECT_PATH # IMPORTANT: Change path to the project location
BATCH=run-LBTC2016-highcwu.sh
RUN_SCRIPT_PATH=src/model/LTBC2016/run-highcwu.py
STATUS=$OUTDIR/status.txt

export TMPDIR=$SCRATCHDIR
export RUN_SCRIPT_PATH=$RUN_SCRIPT_PATH


echo "$PBS_JOBID is running on node `hostname -f` in a scratch directory $SCRATCHDIR" >> $STATUS


# Scratch directory = temporary outputs
# test if scratch directory is set
test -n "$SCRATCHDIR" || { echo >&2 "Variable SCRATCHDIR is not set!"; exit 1; }
cd $SCRATCHDIR


cp -r $INDIR/. $SCRATCHDIR || { echo >&2 "Error while copying input file(s)!"; exit 2; }


singularity run --nv /cvmfs/singularity.metacentrum.cz/NGC/PyTorch\:23.11-py3.SIF ./scripts/singularity-job.sh

#python3 $SCRIPT>output || { echo >&2 "Calculation ended up erroneously (with a code $?) !!"; exit 3; }

# move the output to user's DATADIR or exit in case of failure
cp -r . $OUTDIR/ || { echo >&2 "Result file(s) copying failed (with a code $?) !!"; exit 4; }
echo "Output copied to $OUTDIR" >> $STATUS

# clean the SCRATCH directory
clean_scratch

