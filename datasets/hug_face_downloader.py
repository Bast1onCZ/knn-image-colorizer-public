import datasets
import datasets.utils
import os
import zipfile
import requests
import json
import threading
from PIL import Image


class HugFaceDownloader():
    """Downloader for deeplake datasets."""

    DATASETS = {
        "high-cwu" : {
            "target_data_path" : "/storage/brno2/home/xnevrk03/data_knn/hugging-face/high-cwu",
            "target_labels_path" : "/storage/brno2/home/xnevrk03/data_knn/hugging-face/high-cwu",
            "src_url" : "https://datasets-server.huggingface.co/rows?dataset=HighCWu%2Ffill50k&config=default&split=train"
        }
    }

    def __init__(self, dataset_config : dict) -> None: # Seed is used only when the max size is notne
        self.config = dataset_config
        self.dataset = None
        self.init_dataset_index = None

    def load(self, max_size = None, seed : int = 1):
        self.init_dataset_index = json.loads(requests.request("GET", self.config["src_url"]).content) # TODO: Check the response


    def get(self, workers : int = 2, batch_size : int = 32):
        if self.init_dataset_index is None:
            print(f"Dataset was not loaded...")
            return

        cur_dir = os.path.dirname(os.path.abspath(__file__))
        labels_dir = os.path.join(cur_dir, self.config["target_labels_path"])
        images_dir = os.path.join(cur_dir, self.config["target_data_path"])

        datasets.utils.create_path(os.path.join(cur_dir, self.config["target_labels_path"]))
        datasets.utils.create_path(os.path.join(cur_dir, self.config["target_data_path"]))

        csv_f = open(f"{labels_dir}/labels.csv", "a")
        zip_f = zipfile.ZipFile(f"{images_dir}/images.zip", "a", compression=zipfile.ZIP_DEFLATED)

        cnt = 0

        pages = self.init_dataset_index["num_rows_total"] // self.init_dataset_index["num_rows_per_page"]
        for i in range(pages):

            dataset_index = None
            try:
                dataset_index = json.loads(requests.request("GET", f"{self.config['src_url']}&offset={i}").content) # TODO: Check the response
            except requests.HTTPError:
                    continue

            csv = []
            if not "rows" in dataset_index:
                continue

            for data in dataset_index["rows"]: # TODO: Check the structure of dictionary

                image = None
                try:
                    image = requests.get(f"{data['row']['image']['src']}").content # TODO: Check the response
                except requests.HTTPError:
                    continue

                file_name = str(cnt) + ".jpg"
                csv.append(f"{file_name}, 0\n") # TODO: MAke it more general

                zip_f.writestr(file_name, image)

                cnt += 1

                print(f"Downloading data... [{cnt}/{self.init_dataset_index['num_rows_total']}]", end="\r")

            csv_f.writelines(csv)

        csv_f.close()
        zip_f.close()

        print(f"Downloading data done.\n")

