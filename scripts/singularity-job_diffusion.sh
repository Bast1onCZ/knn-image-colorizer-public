#!/bin/bash

if test -d .git; then
    echo ".git FOUND"
else
    echo ".git NOT FOUND"
fi

if test -d .git; then
    echo "ENV FOUND"
else
    echo "CREATING THE NEW VIRTUAL ENV"
    python3 -m pip install virtualenv
    python3 -m virtualenv env
fi

echo "CUDA RUNTIME VERSION:"
nvcc --version

. ./env/bin/activate

export TMPDIR=$SCRATCHDIR

python3 -m pip install --upgrade pip
pyhton3 -m pip install packaging
pyhton3 -m pip install torch
python3 -m pip install -r requirements.txt

# Fix undefined symbol error
#pip uninstall transformer-engine -y
#pip install transformers

python3 -m pip list

if [ -z $WANDB_API_KEY ]; then
    echo "WAND_KEY NOT DEFINED running wandb loging without it"
    python3 -m wandb login
else
    python3 -m wandb login $WANDB_API_KEY
fi

python3 -m wandb online # python -m wandb offline

echo "Running script ---------------------"

#cd src/model/diffusion
#python train.py --log True --dataset /storage/brno2/home/xnevrk03/data_knn/celeba
#cd src/model/our_diffusion
#python3 src/model/our_diffusion/run.py --dataset /storage/brno2/home/xnevrk03/data_knn/celeba
python3 $RUN_SCRIPT_PATH

echo "Done script ---------------------"
