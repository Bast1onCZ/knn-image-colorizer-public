import torch
from PIL import Image
from skimage.color import lab2rgb
import numpy as np
import matplotlib.pyplot as plt
import warnings

# ------------------------------------------------------------
# Support functions
# ------------------------------------------------------------

def split_lab_channels(image):
    assert isinstance(image, torch.Tensor)
    if len(image.shape) == 3:
        image = image.unsqueeze(0)
    return torch.split(image, [1, 2], dim=1)


def cat_lab(l, ab):
    return (torch.cat((l, ab), dim=1))



def lab_to_pil(img):
    if len(img.shape) == 3:
        img = img.unsqueeze(0)
    
    rgb_img = lab_to_rgb(*split_lab_channels(img))
    pil_img = Image.fromarray(np.uint8(rgb_img[0] * 255))
    return pil_img

def lab_to_rgb(L, ab):
    """
    Converts a batch of torch tensors from Lab to RGB
    """
    L = (L + 1.) * 50.
    ab = ab * 110.
    Lab = torch.cat([L, ab], dim=1).permute(0, 2, 3, 1).cpu().numpy()
    rgb_imgs = []
    for img in Lab:

        ## TODO solve warning
        with warnings.catch_warnings(record=False) as _:
            img_rgb = lab2rgb(img)
        rgb_imgs.append(img_rgb)
    return np.stack(rgb_imgs, axis=0)

def show_lab_image(image, stepsize=10, log=True, caption="diff samples"):
    plt.figure(figsize=(20, 9))
    rgb_imgs = lab_to_rgb(*split_lab_channels(image))
    plt.imshow(rgb_imgs[0])
    #plt.show()

def sample_plot_image(img, diffusion_steps, model, diffusion, show=False, save_paths=None, logger=None):
    x_l = img[:, :1]
    greyscale = torch.cat((x_l, *[torch.zeros_like(x_l)] * 2), dim=1) 

    sampled, generated_image = sample_loop(x_l, diffusion_steps, model, diffusion)

    diffusion_images = [img, greyscale] + sampled
    final_images = [img, greyscale] + generated_image

    diffusion_step_size = max(1, (len(diffusion_images) - 2) // diffusion_steps)
    all_names = [
        ('inference_result', 
            ['Original', 'Greyscale', 'Generated', 
                'Generated A', 'Generated B', 'Generated AB'
            ]), 
        ('diffsuion_steps', 
            ['Original', 'Greyscale'] + 
            [f"Step {i}" for i in range(0, diffusion_steps, diffusion_step_size)])
    ]
    for b in range(img.shape[0]):
        for (title, names), images in zip(all_names, [final_images, diffusion_images]):
            fig, ax = plt.subplots(1, len(images), figsize=(15, 5))
            for i, curr_img in enumerate(images):
                curr_img = lab_to_rgb(*split_lab_channels(curr_img[b]))[0]
                ax[i].imshow(curr_img)
                ax[i].axis('off')
                ax[i].set_title(names[i])
            fig.tight_layout()
            #fig.suptitle(img_name)

            if show:
                plt.show()
            if save_paths[b] is not None:
                fig.savefig(save_paths[b] + f"_{title}.png")
            if logger is not None:
                logger.log({title: fig}) #wandb_imgs})

        # Save generated image
        if save_paths[b] is not None:
            curr_img = lab_to_rgb(*split_lab_channels(final_images[2][b]))[0]
            img = Image.fromarray((curr_img * 255).astype(np.uint8))
            img.save(save_paths[b] + f".png")

def sample_loop(x_l, diffusion_steps, model, diffusion, max_images=5):
    num_images = min(max_images, diffusion_steps)
    stepsize = diffusion_steps // num_images

    # Initialize image with random noise in color channels
    x_ab = torch.randn((x_l.shape[0], 2, x_l.shape[-2], x_l.shape[-1])).to(x_l)
    img = torch.cat((x_l, x_ab), dim=1)

    counter = range(0, diffusion_steps)[::-1]
    
    # Do not recalculate encoder for each diffusion step
    l, _ = split_lab_channels(img)
    greyscale_emb = model.encoder(l)

    images = []
    for i in counter:
        #print(f"    Step: {i}")
        t = torch.full((1,), i, dtype=torch.long).to(img)
        img = diffusion.sample_timestamp(
            model.unet, model.encoder, 
            img, t, grayscale_emb=greyscale_emb
        )
        if i % stepsize == 0 or i == diffusion_steps - 1:
            images += img.unsqueeze(0)

    a_ch = img[:, 1:2, :, :]
    b_ch = img[:, 2:3, :, :]
    a_channel = torch.cat((torch.zeros_like(x_l), a_ch, torch.zeros_like(x_l)), dim=1)
    b_channel = torch.cat((torch.zeros_like(x_l), torch.zeros_like(x_l), b_ch), dim=1)
    ab_channel = torch.cat((torch.zeros_like(x_l), a_ch, b_ch), dim=1)

    final_images = []
    final_images += img.unsqueeze(0)
    final_images += a_channel.unsqueeze(0)
    final_images += b_channel.unsqueeze(0)
    final_images += ab_channel.unsqueeze(0)

    return images, final_images


