import sys

sys.path.append('./')

import torch
import multiprocessing as mp
from datasets.deeplake import DeeplakeDataset
from src.model.LTBC2016.LTBC2016Net import LTBC2016Net
from src.model.LTBC2016.train import train


if __name__ == '__main__':
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    print(f'Running on device: {device}')
    print(f"CUDA-driver version: {torch.version.cuda}")


    mp.set_start_method("spawn")
    dataset = DeeplakeDataset(DeeplakeDataset.DATASETS["places205"])

    train(
        model=LTBC2016Net(global_classes_count=245),  # Although the name of dataset is places 205 there are exactly 245 classes
        dataset=dataset,
        epochs=15,
        log_train_batch_count=20,
        device=device,
        classification_loss_influence_ratio=900,
        optimizer='Adam',
        optimizer_args={
            'lr': 0.0001
        },
        pretrained_model_path=(None if len(sys.argv) < 2 else sys.argv[1])
    )
