import torch
import torch.nn as nn
import torch.nn.functional as f

from common_modules import Residual, PreNorm, LinearAttention, Downsample, ResnetBlock

# ------------------------------------------------------------
# Support Encoder
# ------------------------------------------------------------

class Encoder(nn.Module):
    def __init__(self, in_channels : int = 1):
        super().__init__()

        # ------------------------------------------------------------
        # Parameters
        # ------------------------------------------------------------

        self.RESNET_GROUPS = 8
        self.DROPOUT = 0.2
        self.INNER_CHANNELS = [128, 128, 128 * 2, 128 * 3, 128 * 3]

        # Get pairs (in_ch, out_ch)
        init_ch = self.INNER_CHANNELS[0]
        inner_ch_io = list(zip(self.INNER_CHANNELS[:-1], self.INNER_CHANNELS[1:]))

        # ------------------------------------------------------------
        # Layers
        # ------------------------------------------------------------

        self.init_conv = nn.Conv2d(in_channels, init_ch, 7, padding = 3)

        self.downs = nn.ModuleList([])
        for i, (in_d, out_d) in enumerate(inner_ch_io):
            is_last = i >= (len(inner_ch_io) - 1)

            self.downs.append(nn.ModuleList([
                ResnetBlock(in_d, in_d, groups=self.RESNET_GROUPS),
                ResnetBlock(in_d, in_d, groups=self.RESNET_GROUPS),
                Residual(PreNorm(in_d, LinearAttention(in_d))),
                Downsample(in_d, out_d, dropout=self.DROPOUT) if not is_last else nn.Conv2d(in_d, out_d, 3, padding = 1)
            ]))

    # ------------------------------------------------------------
    # Forward
    # ------------------------------------------------------------

    def forward(self, x):
        x = self.init_conv(x)
        t = None
        h = []

        intermediates = []
        for block1, block2, attn, downsample in self.downs:
            x = block1(x, t)
            h.append(x)
            
            x = block2(x, t)
            x = attn(x)
            h.append(x)

            intermediates.append(x)
            x = downsample(x)

        return intermediates

