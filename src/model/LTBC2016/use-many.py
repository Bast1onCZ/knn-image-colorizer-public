import sys

sys.path.append('./')

import torch
import os
import torchvision
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import csv
from PIL import Image
from src.model.LTBC2016.LTBC2016Net import LTBC2016Net
from datasets.utils import LAB2RGBConversion, RGB2LABConversion, rgb2gray, create_path
from skimage.color import rgb2hsv


def colorfullness(l, a, b):
    n = l.numel()
    return ((torch.sqrt(a**2 + b**2)/torch.sqrt(a**2 + b**2 + l**2)).sum()/n) * 100

def grayscale2l(g):
    return (g - 0.5) * 2.0


model_pth_path = sys.argv[1]
image_dir_path = sys.argv[2]
results_dir_path = sys.argv[3]

create_path(results_dir_path)

# Model setup
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

model = LTBC2016Net(global_classes_count=245)
model.load_state_dict(torch.load(model_pth_path, map_location=device))
model.eval()

result_log = {
    'file' : [],
    'mse' : [],
    'sat' : []
}

for filename in os.listdir(image_dir_path):
    if os.path.isfile(os.path.join(image_dir_path, filename)):
        rgb_input_pil_image = Image.open(os.path.join(image_dir_path, filename))

        # Just for computing MSE
        rgb_input_tensor = torchvision.transforms.ToTensor()(rgb_input_pil_image)
        shrinked_rgb_input_tensor = torchvision.transforms.Resize([112, 112])(rgb_input_tensor)
        ###

        gray_input_pil_image = rgb2gray(rgb_input_pil_image)
        gray_input_tensor = torchvision.transforms.ToTensor()(gray_input_pil_image)

        resized_gray_image_tensor = torchvision.transforms.Resize([224, 224])(gray_input_tensor)
        shrinked_gray_image_tensor = torchvision.transforms.Resize([112, 112])(gray_input_tensor)

        input_tensor = grayscale2l(resized_gray_image_tensor)

        # Inference
        ab, _ = model.forward(input_tensor)

        l = grayscale2l(shrinked_gray_image_tensor[0])
        a = ab[0][0].detach()
        b = ab[0][1].detach()

        lab_image = torch.cat((l.unsqueeze(0), a.unsqueeze(0), b.unsqueeze(0)), dim=0)

        # Convert LAB image to RGB
        rgb_result = LAB2RGBConversion()(lab_image)

        result_log['file'].append(filename)
        result_log['sat'].append(float(colorfullness(l, a, b)))
        result_log['mse'].append(float(torch.nn.functional.mse_loss(rgb_result, shrinked_rgb_input_tensor, reduction='sum')))

        torchvision.transforms.ToPILImage()(rgb_result).save(os.path.join(results_dir_path, filename))

with open(os.path.join(results_dir_path, 'RESULT.csv'), 'w', newline='') as file:
    writer = csv.DictWriter(file, fieldnames=result_log.keys())
    writer.writeheader()
    rows = zip(*result_log.values())
    for row in rows:
        writer.writerow(dict(zip(result_log.keys(), row)))
