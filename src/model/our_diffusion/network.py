import torch
import torch.nn as nn
import torch.nn.functional as f

from unet import UNet
from encoder import Encoder
##from torch_ema import ExponentialMovingAverage

class Network(nn.Module):
    def __init__(self, device, print_model=True):
        super().__init__()

        self.device = device
        self.unet = UNet().to(device)
        self.encoder = Encoder().to(device)
        
        ###self.ema = ExponentialMovingAverage(self.unet.parameters(), decay=0.9999)
        #####self.ema.to(self.device)

        if print_model:
            print("UNet:")
            print(self.unet)
            print("Encoder:")
            print(self.encoder)
            param_cnt = sum(p.numel() for p in self.parameters())
            print(f"Total parameters: {param_cnt}")
        
    def forward(self, x_noised, t, x_l):
        """
        Performs one denoising step on batch of noised inputs
        Unet is conditioned on timestamp and features extracted from grayscale channel
        """
        cond = self.encoder(x_l) 
        noise_prediction = self.unet(x_noised, t, greyscale_embs=cond)
        return noise_prediction
