import sys

sys.path.append('./')

import torch
import multiprocessing as mp
from datasets.static import StaticDataset
from src.model.LTBC2016.LTBC2016Net import LTBC2016Net
from src.model.LTBC2016.train import train


if __name__ == '__main__':
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    print(f'Running on device: {device}')
    print(f"CUDA-driver version: {torch.version.cuda}")


    mp.set_start_method("spawn")
    dataset = StaticDataset(StaticDataset.DATASETS["places205-debug"])

    train(
        model=LTBC2016Net(global_classes_count=245),  # Although the name of dataset is places 205 there are exactly 245 classes
        dataset=dataset,
        epochs=200,
        log_train_batch_count=1,
        device=device,
        classification_loss_influence_ratio=900,
        batch_size=64,
        optimizer='Adam',
        optimizer_args={
            'lr': 0.0001
        }
    )
