import os
import pandas as pd
import datasets.utils
import torch
import zipfile
import torch.utils.data
from PIL import Image
from datasets.deeplake_downloader import DeeplakeDownloader
from datasets.hug_face_downloader import HugFaceDownloader


# USAGE:
# ```
# dataset = StaticDataset(StaticDataset.DATASETS['places205-debug']) # Prepare the dataset
# dataloader = dataset.dataloader() # Get standard pytorch dataloader (specify subset eventually)
# ```


class StaticDataset:
    """Wrapper above standard pytorch dataset. It does the most of thing
    automatically - fetching the new data, splitting dataset etc."""

    TRAIN_TEST_VALID_RATIO = [0.7, 0.15, 0.15]  # Sum must be 1
    DATASETS = {
        "places205-debug": {
            "name": 'places205-debug',
            "images_path": "deeplake/places205-debug/images.zip",
            "labels_path": "deeplake/places205-debug/labels.csv",
            "downloader": DeeplakeDownloader(DeeplakeDownloader.DATASETS["places205-debug"]),
            "l_size": [224, 224],
            "ab_size": [112, 112],
            "collate": datasets.utils.places205_collate,
            "transform": datasets.utils.places205_transform,
            "max_size": 10000,
        },
        "high-cwu": {
            "name": 'high-cwu',
            "images_path": "/storage/brno2/home/xnevrk03/data_knn/hugging-face/high-cwu/images.zip",
            "labels_path": "/storage/brno2/home/xnevrk03/data_knn/hugging-face/high-cwu/labels.csv",
            "downloader": HugFaceDownloader(HugFaceDownloader.DATASETS["high-cwu"]),
            "l_size": [224, 224],
            "ab_size": [112, 112],
            "collate": datasets.utils.places205_collate,
            "transform": datasets.utils.places205_transform,
            "max_size": 40500,
        },
    }

    def __init__(
        self,
        dataset_config: dict,
        max_size=None,
        download: bool = True,
        download_workers: int = 8,
        download_batch_size: int = 1,
        seed: int = 1
    ):
        self.config = dataset_config

        cur_dir = os.path.dirname(os.path.abspath(__file__))
        labels_path = os.path.join(cur_dir, self.config["labels_path"])
        images_dir = os.path.join(cur_dir, self.config["images_path"])

        if max_size is None and "max_size" in self.config:
            max_size = self.config["max_size"]

        self.labels_full_path = labels_path
        self.images_full_path = images_dir

        if not os.path.exists(self.labels_full_path) or not os.path.exists(self.images_full_path):
            if download:
                dataset_config["downloader"].load(max_size)
                dataset_config["downloader"].get(workers=download_workers, batch_size=download_batch_size)
            else:
                print("Data are not downloaded! Please allow download")
                return

        labels = pd.read_csv(self.labels_full_path, header=None)
        if max_size is not None and len(labels) < max_size:
            labels = self._extend(download, max_size, download_workers, download_batch_size)
            if labels is None:
                return

        self.dataset = StaticDatasetWrapper(
            labels,
            self.images_full_path,
            self.config["transform"],
            {
                "l_size": self.config["l_size"],
                "ab_size": self.config["ab_size"]
            }
        )

        # Split dataset
        generator = torch.Generator().manual_seed(seed)  # Truncate it if necessary
        base_dataset = self.dataset
        if max_size is not None and max_size < len(self.dataset):
            base_dataset, _ = torch.utils.data.random_split(
                self.dataset,
                [max_size, len(self.dataset) - max_size],
                generator
            )

        dataset_split = torch.utils.data.random_split(  # Split it to training, test and validation part
            base_dataset,
            self.TRAIN_TEST_VALID_RATIO,
            generator
        )

        self.train_dataset = dataset_split[0]
        self.test_dataset = dataset_split[1]
        self.valid_dataset = dataset_split[2]
        print(f"Training dset size: {len(self.train_dataset)}")
        print(f"Testing dset size: {len(self.test_dataset)}")
        print(f"Validation dset size: {len(self.valid_dataset)}")

    def _extend(self, download, max_size, download_workers, download_batch_size):
        if download:
            print(f"Extending dataset...")
            self.config["downloader"].load(max_size)
            self.config["downloader"].get(workers=download_workers, batch_size=download_batch_size)
            return pd.read_csv(self.labels_full_path, header=None)
        else:
            print(f"There are not enough data! Please allow download to extend the dataset")
            return None

    def dataloader(
            self,
            type: str = "train",
            shuffle : bool = False,
            pin_memory: bool = False,
            batch_size : int = 32,
            workers_num : int = 2,
            augmented: bool = False
        ):

        self.dataset.transform_kwargs["augment"] = augmented

        dataset_subset = self.train_dataset
        if type == "test":
            dataset_subset = self.test_dataset
        elif type == "valid":
            dataset_subset = self.valid_dataset

        return torch.utils.data.DataLoader(
            dataset_subset,
            batch_size=batch_size,
            shuffle=shuffle,
            num_workers=workers_num,
            pin_memory=pin_memory,
            collate_fn=self.config["collate"]
        )


class StaticDatasetWrapper(torch.utils.data.Dataset):
    def __init__(self, labels, image_path, transform, transform_kwargs) -> None:
        self.labels = labels
        self.image_path = image_path
        self.transform = transform
        self.transform_kwargs = transform_kwargs

    def __len__(self):
        return len(self.labels)

    def __getitem__(self, idx):
        archive = zipfile.ZipFile(self.image_path)
        arch_img = archive.open(self.labels.iloc[idx, 0])
        image = Image.open(arch_img)
        archive.close()

        label = self.labels.iloc[idx, 1]

        if self.transform:
            data = self.transform({
                "images": image,
                "labels": label,
                "index": idx
            }, **self.transform_kwargs)
            return data

        return {"images": image, "labels": label, "index": idx}


def send_to_device(data, device):
    return {
        "l" : data["l"].to(device),
        "ab" : data["ab"].to(device),
        "labels" : data["labels"].to(device)
    }


class DataloaderDeviceWrapper:
    def __init__(
            self,
            dataloader : torch.utils.data.DataLoader,
            device : torch.device
        ) -> None:
        self.dataloader = dataloader
        self.func = send_to_device
        self.device = device

    def __len__(self):
        return len(self.dataloader)

    def __iter__(self):
        for i in self.dataloader:
            yield (self.func(i, self.device))
