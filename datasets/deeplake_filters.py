import deeplake

class Places205FilterNoGrayMini():
    def __init__(self, classes, max_size_per_class) -> None:
        self.classes_dict = {c : 0 for c in classes}
        self.max_size_per_class = max_size_per_class


@deeplake.compute
def places205_filter_no_gray(sample):
    return sample.images.data()["value"].shape[2] == 3

@deeplake.compute
def places205_filter_no_gray_red(sample):
    classes = ["forest_path", "canyon"]

    return len(sample.labels.data()["text"]) and sample.labels.data()["text"][0] in classes and (sample.images.data()["value"].shape[2] == 3)


@deeplake.compute
def places205_filter_no_gray_5classes(sample):
    classes = ["forest_path", "canyon", "abbey", "kitchen", "bridge"]

    return len(sample.labels.data()["text"]) and sample.labels.data()["text"][0] in classes and (sample.images.data()["value"].shape[2] == 3)


@deeplake.compute
def places205_filter_no_gray_mini(sample, ctx=Places205FilterNoGrayMini(["forest_path", "canyon", "mansion"], 128)):

    is_in_classes = len(sample.labels.data()["text"]) and sample.labels.data()["text"][0] in ctx.classes_dict and (sample.images.data()["value"].shape[2] == 3)
    if not is_in_classes:
        return False

    is_class_full = ctx.classes_dict[sample.labels.data()["text"][0]] >= ctx.max_size_per_class
    ctx.classes_dict[sample.labels.data()["text"][0]] += 1

    return not is_class_full
