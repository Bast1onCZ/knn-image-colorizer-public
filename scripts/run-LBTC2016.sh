#!/bin/bash
#PBS -N gpu_training
#PBS -q gpu
#PBS -l select=1:ngpus=1:ncpus=24:mem=16gb:scratch_local=20gb:gpu_mem=16gb:gpu_cap=cuda60
#PBS -l walltime=23:30:00
#PBS -m ae

source ~/.profile

PROJECT_PATH="$PBS_O_WORKDIR"

INDIR=$PROJECT_PATH
OUTDIR=$PROJECT_PATH
BATCH=run-LBTC2016.sh
RUN_SCRIPT_PATH=src/model/LTBC2016/run.py
STATUS=$OUTDIR/status.txt

export TMPDIR=$SCRATCHDIR
export RUN_SCRIPT_PATH=$RUN_SCRIPT_PATH


echo "$PBS_JOBID is running on node `hostname -f` in a scratch directory $SCRATCHDIR" >> $STATUS


# Scratch directory = temporary outputs
# test if scratch directory is set
test -n "$SCRATCHDIR" || { echo >&2 "Variable SCRATCHDIR is not set!"; exit 1; }
cd $SCRATCHDIR


cp -r $INDIR/. $SCRATCHDIR || { echo >&2 "Error while copying input file(s)!"; exit 2; }


singularity run --nv /cvmfs/singularity.metacentrum.cz/NGC/PyTorch\:23.11-py3.SIF ./scripts/singularity-job.sh

#python3 $SCRIPT>output || { echo >&2 "Calculation ended up erroneously (with a code $?) !!"; exit 3; }

# move the output to user's DATADIR or exit in case of failure
cp -r . $OUTDIR/ || { echo >&2 "Result file(s) copying failed (with a code $?) !!"; exit 4; }
echo "Output copied to $OUTDIR" >> $STATUS

# clean the SCRATCH directory
clean_scratch

