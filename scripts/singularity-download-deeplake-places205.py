# Downloads FULL places205 dataset in deeplake-specific format
# DEEPLAKE_DOWNLOAD_PATH need to be set

import deeplake

deeplake.load(
    path="hub://activeloop/places205",
    access_method="download:16"
)


