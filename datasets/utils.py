import torch
import torchvision
import deeplake
from PIL import ImageCms
from skimage.color import rgb2lab, lab2rgb
import numpy as np
import os

class RGB2LABConversion():
    """Class responsible for RGB to CIELAB conversion. Expects [0,1] on the
    input (std. format returned by e.g torchvision.transforms.ToTensor()) and
    returns [-1,1] to the output."""

    def __init__(self) -> None:
        pass

    def __call__(self, image_tensor):
        base_img = np.array(image_tensor.permute(1,2,0))
        img_lab = rgb2lab(base_img).astype("float32")
        img_lab = torchvision.transforms.ToTensor()(img_lab)
        l = img_lab[[0]] / 50.0 - 1.0 # range [0,100] to [-1, 1]
        ab = img_lab[[1, 2]] / 128.0 # range [-128,128] to [-1, 1]
        return torch.cat((l, ab), dim=0)

    def forward(self, image):
        return self.__call__(image)


class LAB2RGBConversion():
    """Class responsible for CIELAB to RGB conversion. Just for reconstruction
    of the transformed image. Expects [-1,1] on the
    input and returns [0,1] to the output.
    """

    def __init__(self) -> None:
        pass

    def __call__(self, image_tensor):
        l = image_tensor[[0]]
        ab = image_tensor[[1, 2]]
        l = (l + 1.0) * 50.0 # [-1,1] to [0,100]
        ab = ab * 128.0 # [-1, 1] to [-128, 128]
        lab = torch.cat((l, ab), dim=0)
        base_img = np.array(lab.permute(1,2,0))
        img_rgb = lab2rgb(base_img).astype("float32")
        img_rgb = torchvision.transforms.ToTensor()(img_rgb)
        return img_rgb

    def forward(self, image):
        return self.__call__(image)



def places205_transform(data, **kwargs):
    """Transformation of data in places205 dataset."""

    image = data["images"]

    if image.mode == "L":
        data["is_gray"] = True
    else:
        data["is_gray"] = False

    input_to_lab = torchvision.transforms.Compose([
        torchvision.transforms.ToTensor(),
        torchvision.transforms.RandomCrop(kwargs['l_size']),
        RGB2LABConversion(),
    ])

    shrink_ab = torchvision.transforms.Compose([
        torchvision.transforms.Resize(kwargs['ab_size'])
    ])

    if not data["is_gray"]:
        lab = input_to_lab(image)
        if "augment" in kwargs and kwargs["augment"]:
            lab = torchvision.transforms.RandomRotation(degrees=(0, 180))(lab)

        data["l"] = lab[0,:,:].float()
        data["ab"] = shrink_ab(lab[[1, 2],:,:]).float()
        data["labels"] = torch.tensor(data["labels"])

        #assert data['l'].size() == (224, 224)
        #assert data['ab'].size() == (2, 112, 112)
        assert data['labels'].size() == torch.tensor(1).size()
    else:
        gray_input_transf = torchvision.transforms.Compose([
            torchvision.transforms.ToTensor(),
            torchvision.transforms.RandomCrop(kwargs['l_size']),
        ])

        data["l"] = gray_input_transf(image)[0,:,:].float()
        data["ab"] = shrink_ab(torch.zeros((2, 256, 256), dtype=torch.float))
        data["labels"] = torch.tensor(data["labels"])

    return data



def full_places205_transform(data, **kwargs):
    """Transformation of data in places205 dataset."""

    image = data["images"]

    input_to_lab = torchvision.transforms.Compose([
        torchvision.transforms.ToTensor(),
        torchvision.transforms.RandomCrop(kwargs['l_size']),
        RGB2LABConversion(),
    ])

    shrink_ab = torchvision.transforms.Compose([
        torchvision.transforms.Resize(kwargs['ab_size'])
    ])

    lab = input_to_lab(image)

    if "augment" in kwargs and kwargs["augment"]:
        lab = torchvision.transforms.RandomRotation(degrees=(0, 180))(lab)

    data["l"] = lab[0,:,:].float()
    data["ab"] = shrink_ab(lab[[1, 2],:,:]).float()
    data['labels'] = torch.tensor(data['labels'][0])

    #assert data['l'].size() == (224, 224)
    #assert data['ab'].size() == (2, 112, 112)
    assert data['labels'].size() == torch.tensor(1).size()

    return data



def places205_collate(batch):
    """Collate operation for places205 dataset."""

    l = [x["l"] for x in batch]
    ab = [x["ab"] for x in batch]
    labels = [x["labels"] for x in batch]

    return {
        #"images" : [x["images"] for x in batch],
        "l" : torch.stack(l),
        "ab" : torch.stack(ab),
        "labels" : torch.stack(labels),
        "is_gray" : [x["is_gray"] for x in batch],
        "index" : [x["index"] for x in batch]
    }


def full_places205_collate(batch):
    """Collate operation for places205 dataset."""

    l = [x["l"] for x in batch]
    ab = [x["ab"] for x in batch]
    labels = [x["labels"] for x in batch]

    return {
        #"images" : [x["images"] for x in batch],
        "l" : torch.stack(l),
        "ab" : torch.stack(ab),
        "labels" : torch.stack(labels),
        "index" : [x["index"] for x in batch]
    }



def places205_downloader_collate(batch):
    """Collate operation for places205 dataset."""

    return {
        "images" : [x["images"] for x in batch],
        "labels" : [x["labels"] for x in batch],
        "index" : [x["index"] for x in batch]
    }


def rgb2gray(image):
    return image.convert("L")


def create_path(path):
    if not os.path.exists(path):
        os.makedirs(path)
