# Image colorizer

## Authors

Miroslav Klobáska (xkloba01)

Vojtěch Dvořák (xdvora3o)

Lukáš Nevrkla (xnevrk03)

## About

The main objective of this project is to create solution using neural networks that is able to reconstruct color in grayscale images and return colorized version of these images as a result.


## File structure

- `datasets/` ... our wrappers above datasets with our transformations, filtering (data are provided by `deeplake` module)
    - `deeplake.py` ... wrapper above datasets provided by `deeplake` module
    - `deeplake_filters.py` ... filtering functions
    - `deeplake_downloader.py, hug_face_downloader.py` ... classes and functions for fetching the data to be available locally
    - `static.py` ... wrapper above `static` (local) datasets
    - `utils.py` ... auxiliary functions for datasets module
- `scripts/` ... scripts for MetaCentrum
    - `run_diffusion.sh` ... train generative diffusion model
    - `run_inference_diffusion.sh` ... run inference of diffusion model
- `src/model/`
    - `diffusion/` ... edited version of baseline for diffusion model (https://github.com/ErwannMillon/Color-diffusion.git)
    - `our_diffusion/` ... **our implementation of the diffusion model**
        - `run.py` ... **train** diffusion model
        - `inference.py` ... run **inference** of diffusion model
        - `trainer.py` ... code responsible for training
        - `network.py`, `unet.py`, `encoder.py` ... model architecture
        - `diffusion.py` ... forward and backward diffusion process
        - `common.py, common_modules.py` ... other code
    - `LTBC2016/` ... baseline model (based on paper Let Be The Color 2016)
        - `LTBC2016Net.py` ... implementation of neural network
        - `run.py` ... script for training of the model
        - `train.py` ... implementation of training of LBTC2016 model
        - `use.py, use-many.py` ... scripts for inference
- `requirements.txt`


## Usage

All requirements can be installen by `pip` package manager by command:

```
pip install -r requirements.txt
```
(please note, that `python3` and `pip` is required)


Although training scripts are available (more infor about training can be found in docs), we recommend to download our models from URLs below.
After that downloading dataset and training (which may be resource-intensive task) can be skipped.

### Download datasets

There are more than one datasets, that can be used for training. Datasets are downloaded completely automatically, but it is necessary to edit
paths in `static.py` or/and `deeplake.py` source files, especially `"local"` in `DATASETS` class attribute. This path is path to directory where will
be the dataset saved locally. Other properties should not necessary to be changed, because training scripts and models are prepared for these datasets
with the current configuration. After that dataset can be used in training scripts by instantiation of `DeeplakeDataset` or `StaticDataset` classes:

```
dataset = DeeplakeDataset(DeeplakeDataset.DATASETS["places205"])
```

```
dataset = StaticDataset(StaticDataset.DATASETS["places205-debug"])
```



### Train baseline model

1. [Download dataset](#download-dataset)
2. Login to WANDB ` wandb login [USER_API_KEY]`
3. Configure `src/model/LBTC2016/run.py`
4. Run `python3 src/model/LBTC2016/run.py`


### How to run inference for baseline (LTBC2016) model

1. Download one of the models from:
    - https://vutbr-my.sharepoint.com/:f:/g/personal/xdvora3o_vutbr_cz/EtipLBGI9LNCiR6n-rU_1tQB1L2PLzsRFRJRnIA97wDyIA?e=9ODhLu
2. Run script for inference of one image by:
    ```
    python3 src/modelsLTBC2016/use.py <path-to-model> <path-to-rgb-image>
    ```

    OR run script for inference of many image:
    ```
    python3 src/modelsLTBC2016/use-many.py <path-to-model> <path-to-dir-with-rgb-images> <path-to-target-dir>
    ```


(please note, that images have to be colored and in dimensions 224x224)


### Train diffusion model

1. [Download dataset](#download-dataset)
2. Login to WANDB ` wandb login [USER_API_KEY]`
3. Configure `src/model/our_diffusion/run.py`
4. Run `python3 src/model/our_diffusion/run.py`

### Run inference for diffusion model

1. Download model from:
    - https://vutbr-my.sharepoint.com/:u:/g/personal/xnevrk03_vutbr_cz/EWCqbMfGjVRCgGTKbNVbPdEB3aAoABIkGut9rGemxprT0Q?e=MktBm8
2. Save model to `artifacts/best_model:v24/model.pth`
3. Place required images to `test_imgs/` directory (you can make subdirectories)
4. Run `python3 src/model/our_diffusion/inference.py`
5. Result images will show up and will be saved to `res_imgs/` directory
