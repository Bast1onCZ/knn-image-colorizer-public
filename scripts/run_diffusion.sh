#!/bin/bash
#PBS -N gpu_training_debug
#PBS -q gpu
#PBS -l select=1:ngpus=1:ncpus=32:mem=16gb:scratch_local=20gb:gpu_mem=30gb
#PBS -l walltime=24:00:00
#PBS -m ae

# IMPORTANT: Change path to the project location
PROJECT_PATH=/storage/brno2/home/xnevrk03/knn-image-colorizer-public
#DATA_PATH=/storage/brno2/home/xnevrk03/data_knn/celeba
HOMEDIR=/storage/brno2/home/xnevrk03

INDIR=$PROJECT_PATH 
OUTDIR=$PROJECT_PATH/output
RUN_SCRIPT_PATH="src/model/our_diffusion/run.py"
#RUN_SCRIPT_PATH="src/model/our_diffusion/inference.py"
STATUS=$OUTDIR/status.txt

#export TMPDIR=$SCRATCHDIR
export RUN_SCRIPT_PATH=$RUN_SCRIPT_PATH
export WANDB_API_KEY=dc217e9a435c36d812a1b0a355b85733e8e15e48

export TMPDIR=$SCRATCHDIR

echo "$PBS_JOBID is running on node `hostname -f` in a scratch directory $SCRATCHDIR" >> $STATUS

echo "Running"

# Scratch directory = temporary outputs
# test if scratch directory is set
test -n "$SCRATCHDIR" || { echo >&2 "Variable SCRATCHDIR is not set!"; exit 1; }
cd $SCRATCHDIR

#cp -r $INDIR/ $SCRATCHDIR || { echo >&2 "Error while copying input file(s)!"; exit 2; }
rsync -aqv $INDIR/ $SCRATCHDIR --exclude output/ || { echo >&2 "Input file(s) copying failed (with a code $?) !!"; exit 4; }

if [ -d "knn-image-colorizer-public" ]; then
    cd knn-image-colorizer-public
fi

#set SINGULARITY variables for runtime data
export SINGULARITY_CACHEDIR=$HOMEDIR
export SINGULARITY_LOCALCACHEDIR=$SCRATCHDIR

echo "Run singularity"

# TODO --bind /storage/
#singularity run -B $PROJECT_PATH/scripts/singularity-job_diffusion.sh:/home/xnevrk03/singularity-job_diffusion.sh -nv /cvmfs/singularity.metacentrum.cz/NGC/PyTorch\:23.11-py3.SIF ./singularity-job_diffusion.sh
singularity run --nv /cvmfs/singularity.metacentrum.cz/NGC/PyTorch\:23.11-py3.SIF ./scripts/singularity-job_diffusion.sh

echo "Singularity DONE" >> $STATUS
echo "Done singularity"

#python3 $SCRIPT>output || { echo >&2 "Calculation ended up erroneously (with a code $?) !!"; exit 3; }

# move the output to user's DATADIR or exit in case of failure
#cp -r * $OUTDIR/ || { echo >&2 "Result file(s) copying failed (with a code $?) !!"; exit 4; }
rsync -aqv * $OUTDIR/ --exclude .git/ || { echo >&2 "Result file(s) copying failed (with a code $?) !!"; exit 4; }
echo "Output copied to $OUTDIR" >> $STATUS

# clean the SCRATCH directory
clean_scratch

