import torch
import torch.nn as nn
import torch.nn.functional as f


class LTBC2016Net(nn.Module):
    """
    LTBC2016 stands for 1:1 replication of model proposed in paper let-there-be-color-2016.pdf
    """

    def __init__(self, global_classes_count: int = 205, classification_output: bool = True):
        """

        :param global_classes_count:
        :param classification_output: True if network should output also classification softmax Tensor[global_classes_count] for each image. False may be used to optimize production build of network.
        """
        super().__init__()

        self.global_classes_count = global_classes_count
        self.classification_output = classification_output

        NORMAL_STRIDE = 1
        POOLING_STRIDE = 2
        HIDDEN_ACTIVATION = nn.ReLU()

        self.low_level_features_network = nn.Sequential(
            nn.Conv2d(in_channels=1, out_channels=64, kernel_size=3, stride=POOLING_STRIDE, padding=1),
            nn.BatchNorm2d(64),
            HIDDEN_ACTIVATION,
            nn.Conv2d(in_channels=64, out_channels=128, kernel_size=3, stride=NORMAL_STRIDE, padding=1),
            nn.BatchNorm2d(128),
            HIDDEN_ACTIVATION,
            nn.Conv2d(in_channels=128, out_channels=128, kernel_size=3, stride=POOLING_STRIDE, padding=1),
            nn.BatchNorm2d(128),
            HIDDEN_ACTIVATION,
            nn.Conv2d(in_channels=128, out_channels=256, kernel_size=3, stride=NORMAL_STRIDE, padding=1),
            nn.BatchNorm2d(256),
            HIDDEN_ACTIVATION,
            nn.Conv2d(in_channels=256, out_channels=256, kernel_size=3, stride=POOLING_STRIDE, padding=1),
            nn.BatchNorm2d(256),
            HIDDEN_ACTIVATION,
            nn.Conv2d(in_channels=256, out_channels=512, kernel_size=3, stride=NORMAL_STRIDE, padding=1),
            nn.BatchNorm2d(512),
            HIDDEN_ACTIVATION,
        )

        self.mid_level_features_network = nn.Sequential(
            # Input: low_level_features_network(1xWxH)
            nn.Conv2d(in_channels=512, out_channels=512, kernel_size=3, stride=NORMAL_STRIDE, padding=1),
            nn.BatchNorm2d(512),
            HIDDEN_ACTIVATION,
            nn.Conv2d(in_channels=512, out_channels=256, kernel_size=3, stride=NORMAL_STRIDE, padding=1),
            nn.BatchNorm2d(256),
            HIDDEN_ACTIVATION,
        )

        self.global_features_network = nn.Sequential(
            # Input: low_level_features_network(1x224x224)
            nn.Conv2d(in_channels=512, out_channels=512, kernel_size=3, stride=POOLING_STRIDE, padding=1),
            nn.BatchNorm2d(512),
            HIDDEN_ACTIVATION,
            nn.Conv2d(in_channels=512, out_channels=512, kernel_size=3, stride=NORMAL_STRIDE, padding=1),
            nn.BatchNorm2d(512),
            HIDDEN_ACTIVATION,
            nn.Conv2d(in_channels=512, out_channels=512, kernel_size=3, stride=POOLING_STRIDE, padding=1),
            nn.BatchNorm2d(512),
            HIDDEN_ACTIVATION,
            nn.Conv2d(in_channels=512, out_channels=512, kernel_size=3, stride=NORMAL_STRIDE, padding=1),
            nn.BatchNorm2d(512),
            HIDDEN_ACTIVATION,
            nn.Flatten(),
            nn.Linear(in_features=512 * 7 * 7, out_features=1024),
            nn.BatchNorm1d(1024),
            HIDDEN_ACTIVATION,
            nn.Linear(in_features=1024, out_features=512),
            nn.BatchNorm1d(512),
            HIDDEN_ACTIVATION,
        )

        # Input: global_features_network
        self.global_features_prefusion_network = nn.Sequential(
            nn.Linear(in_features=512, out_features=256),
            nn.BatchNorm1d(256),
            HIDDEN_ACTIVATION,
        )

        self.classification_network = nn.Sequential(
            # Input: global_features_network
            nn.Linear(in_features=512, out_features=256),
            nn.BatchNorm1d(256),
            HIDDEN_ACTIVATION,
            nn.Linear(in_features=256, out_features=global_classes_count),
            nn.Softmax(dim=1)
        )

        self.colorization_network = nn.Sequential(
            # Input: Fusion(mid_level_features, global_features_prefusio)
            nn.Conv2d(in_channels=512, out_channels=256, kernel_size=1, stride=NORMAL_STRIDE),
            nn.BatchNorm2d(256),
            HIDDEN_ACTIVATION,
            nn.Conv2d(in_channels=256, out_channels=128, kernel_size=3, stride=NORMAL_STRIDE, padding=1),
            nn.BatchNorm2d(128),
            HIDDEN_ACTIVATION,
            nn.UpsamplingNearest2d(scale_factor=2),
            nn.Conv2d(in_channels=128, out_channels=64, kernel_size=3, stride=NORMAL_STRIDE, padding=1),
            nn.BatchNorm2d(64),
            HIDDEN_ACTIVATION,
            nn.Conv2d(in_channels=64, out_channels=64, kernel_size=3, stride=NORMAL_STRIDE, padding=1),
            nn.BatchNorm2d(64),
            HIDDEN_ACTIVATION,
            nn.UpsamplingNearest2d(scale_factor=2),
            nn.Conv2d(in_channels=64, out_channels=32, kernel_size=3, stride=NORMAL_STRIDE, padding=1),
            nn.BatchNorm2d(32),
            HIDDEN_ACTIVATION,
            nn.Conv2d(in_channels=32, out_channels=2, kernel_size=3, stride=NORMAL_STRIDE, padding=1),
            nn.Sigmoid(),
        )

    def forward(self, images: torch.Tensor):
        """
        :param images: Tensor[batch, height, width]
        :return:
        """

        batch_count, height, width = images.shape

        images = images.unsqueeze(1)  # Tensor[batch, height, width] => Tensor[batch, channel=1, height, width]

        mid_level_features: torch.Tensor  # Tensor[batch, channels=256, height, width]

        if height == width == 224:
            # For training it is more optimal to use all images of size (224x224), so that low_level_features_network may be shared
            low_level_features = self.low_level_features_network(images)
            mid_level_features = self.mid_level_features_network(low_level_features)
            global_features = self.global_features_network(low_level_features)
        else:
            low_level_features__to_mid_level_features_network = self.low_level_features_network(images)
            mid_level_features = low_level_features__to_mid_level_features_network

            fixed_size_images = images.unsqueeze(1)  # Tensor[batch, height, width] => Tensor[batch, channel=1, height, width]
            fixed_size_images = f.interpolate(fixed_size_images, size=(224, 224), mode='bilinear')  # Tensor[batch, channel=1, height, width] => Tensor[batch, channel=1, height=224, width=224]
            fixed_size_images = fixed_size_images.squeeze(1)  # Tensor[batch, channel=1, height=224, width=224] => Tensor[batch, height=224, width=224]

            low_level_features__to_global_features_network = self.low_level_features_network(fixed_size_images)
            global_features = self.global_features_network(low_level_features__to_global_features_network)

        _, _, middle_features_height, middle_features_width = mid_level_features.size()  # Tensor[batch, channels=256, middle_features_height, middle_features_width]
        mid_level_features = mid_level_features.permute([0, 2, 3, 1])  # Tensor[batch, channels=256, middle_features_height, middle_features_width] => # Tensor[batch, middle_features_height, middle_features_width, channels=256]

        global_features_prefusion = self.global_features_prefusion_network(global_features)  # Tensor[batch, channels=256]
        global_features_prefusion = global_features_prefusion.unsqueeze(1).unsqueeze(2)  # Tensor[batch, channels=256] => # Tensor[batch, 1, 1, channels=256]
        global_features_prefusion = global_features_prefusion.repeat(1, middle_features_height, middle_features_width, 1)  # Tensor[batch, 1, 1, channels=256] => # Tensor[batch, middle_features_height, middle_features_width, channels=256]

        fused_features = torch.cat([mid_level_features, global_features_prefusion], dim=3)  # Tensor[batch, middle_features_height, middle_features_width, channels=512]
        fused_features = fused_features.permute([0, 3, 1, 2])  # Tensor[batch, middle_features_height, middle_features_width, channels=512] => # Tensor[batch, channels=512, middle_features_height, middle_features_width]

        colored_image_output = (self.colorization_network(fused_features) - 0.5) * 2  # (a,b) parts are normalized <0,1> -> <-1,1>

        if self.classification_output:
            classification_output = self.classification_network(global_features)

            return colored_image_output, classification_output
        else:
            return colored_image_output
