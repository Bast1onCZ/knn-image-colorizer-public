import deeplake
import os
import torch.utils.data

import datasets.utils
import datasets.deeplake_filters


# USAGE:
# ```
# dataset = DeeplakeDataset(DeeplakeDataset.DATASETS["places205"]) # Prepare the dataset
# dataloader = dataset.dataloader() # Get standard pytorch dataloader (specify subset eventually)
# ```



class DeeplakeDataset():
    """Wrapper above deeplake datasets."""

    TRAIN_TEST_VALID_RATIO = [0.8, 0.1, 0.1] # Sum must be 1
    DATASETS = {
        "places205" : {
            "name" : "places205-no-gray",
            "repo_path" : "hub_activeloop_places205",
            "local" : "/storage/brno2/home/xnevrk03/data_knn",
            "src_path": "hub://activeloop/places205",
            "tensors" : ["images", "labels"],
            "l_size" : [224, 224],
            "ab_size" : [112, 112],
            "view_id" : "no_gray",
            "filter" : datasets.deeplake_filters.places205_filter_no_gray(),
            "collate" : datasets.utils.full_places205_collate,
            "transform" : datasets.utils.full_places205_transform,
        },
        "places205-reduced" : {
            "name" : "places205-no-gray-reduced",
            "repo_path" : "hub_activeloop_places205",
            "local" : "/storage/brno2/home/xnevrk03/data_knn",
            "src_path": "hub://activeloop/places205",
            "tensors" : ["images", "labels"],
            "l_size" : [224, 224],
            "ab_size" : [112, 112],
            "view_id" : "no-gray-reduced",
            "filter" : datasets.deeplake_filters.places205_filter_no_gray_red(),
            "collate" : datasets.utils.full_places205_collate,
            "transform" : datasets.utils.full_places205_transform,
        },
        "places205-mini" : {
            "name" : "places205-no-gray-mini",
            "repo_path" : "hub_activeloop_places205",
            "local" : "/storage/brno2/home/xnevrk03/data_knn",
            "src_path": "hub://activeloop/places205",
            "tensors" : ["images", "labels"],
            "l_size" : [224, 224],
            "ab_size" : [112, 112],
            "view_id" : "no-gray-mini",
            "filter" : datasets.deeplake_filters.places205_filter_no_gray_mini(),
            "collate" : datasets.utils.full_places205_collate,
            "transform" : datasets.utils.full_places205_transform,
        },
        "places205-5classes" : {
            "name" : "places205-no-gray-5classes",
            "repo_path" : "hub_activeloop_places205",
            "local" : "/storage/brno2/home/xnevrk03/data_knn",
            "src_path": "hub://activeloop/places205",
            "tensors" : ["images", "labels"],
            "l_size" : [224, 224],
            "ab_size" : [112, 112],
            "view_id" : "no-gray-5classes",
            "filter" : datasets.deeplake_filters.places205_filter_no_gray_5classes(),
            "collate" : datasets.utils.full_places205_collate,
            "transform" : datasets.utils.full_places205_transform,
        },
    }

    def __init__(self, dataset_config : dict, download : bool = False, workers_num : int = 8, seed : int = 1) -> None:
        deeplake.random.seed(seed)

        self.config = dataset_config
        self.dataset_local_path = os.path.join(
            self.config["local"],
            self.config["repo_path"]
        )

        self.base_dataset = None
        self.view = None

        if not deeplake.exists(self.dataset_local_path) and download:
            self.base_dataset = deeplake.deepcopy(
                self.config["src_path"],
                self.config["local"],
                tensors=self.config["tensors"]
            )
        elif deeplake.exists(self.dataset_local_path):
            try:
                self.base_dataset = deeplake.load(
                    self.dataset_local_path,
                    verbose=True,
                    reset=True
                )
            except PermissionError: # Repeat if there were permission error
                self.base_dataset = deeplake.load(self.dataset_local_path, verbose=True, reset=True)
        else:
            print(f"Dataset repository {self.config['repo_path']} ({self.dataset_local_path}) does not exists!")
            print(f"Please allow download to download it from {self.config['src_path']}.")

        self._load_view(workers_num)

        base_dataset = self.view if self.view is not None else self.base_dataset
        self.dataset = base_dataset

        dataset_split = self.dataset.random_split(
            self.TRAIN_TEST_VALID_RATIO,
        )

        self.train_dataset = dataset_split[0]
        self.test_dataset = dataset_split[1]
        self.valid_dataset = dataset_split[2]
        print(f"Training dset size: {len(self.train_dataset)}")
        print(f"Testing dset size: {len(self.test_dataset)}")
        print(f"Validation dset size: {len(self.valid_dataset)}")


    def _load_view(self, workers_num : int = 0):
        if "view_id" in self.config:
            try:
                self.view = self.base_dataset.get_view(self.config["view_id"]).load()
            except KeyError:
                self.view = self.base_dataset.filter(
                    self.config["filter"],
                    scheduler = "threaded",
                    num_workers = workers_num
                )

                self.view.save_view(id=self.config["view_id"])

        if self.view is not None:
            print(f"Loaded view '{self.config['view_id']}' with {len(self.view)} samples (from: {len(self.base_dataset)})")


    def dataloader(
            self,
            type: str = "train",
            shuffle : bool = False,
            pin_memory: bool = False,
            batch_size : int = 32,
            workers_num : int = 2,
            buffer_size : int = 1024,
            augmented : bool = False
        ):

        """Creates pytorch dataloader from the dataset.

        :param str type: Type of dataset (one of "train","test","valid")

        IMPORTANT:
        In some datasets are images grouped by classes. So if training is not
        long enough, some classes can be ommited. Use shuffle flag to get
        random samples.
        """

        dataset_subset = self.train_dataset
        if type == "test":
            dataset_subset = self.test_dataset
        elif type == "valid":
            dataset_subset = self.valid_dataset

        return dataset_subset.pytorch(
            num_workers=workers_num,
            batch_size=batch_size,
            shuffle=shuffle,
            tensors= self.config["tensors"],
            transform=self.config["transform"],
            decode_method={"images" : "numpy"},
            collate_fn=self.config["collate"],
            transform_kwargs={
                "l_size" : self.config["l_size"],
                "ab_size" : self.config["ab_size"],
                "augment" : augmented,
            },
            buffer_size=buffer_size,
            pin_memory=pin_memory
        )

