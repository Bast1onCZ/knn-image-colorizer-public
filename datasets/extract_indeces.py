import sys
import os
import zipfile
import pandas as pd

archive = zipfile.ZipFile(sys.argv[1])
labels = pd.read_csv(sys.argv[2], header=None)
indeces = pd.read_csv(sys.argv[3], header=None)

new_arch = zipfile.ZipFile(sys.argv[4], 'w')
for i in indeces[0]:
    arch_img = archive.read(labels.iloc[i, 0])
    new_arch.writestr(labels.iloc[i, 0], arch_img)

new_arch.close()
