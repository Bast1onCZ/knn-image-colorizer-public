from copy import deepcopy

import git
import torch.optim as optim
from torch import Tensor

from torch.utils.data import Dataset, DataLoader

from datasets.static import DataloaderDeviceWrapper, StaticDataset
from datasets.deeplake import DeeplakeDataset
from datasets.utils import LAB2RGBConversion
from src.model.LTBC2016.LTBC2016Net import LTBC2016Net
import torch
import torchvision.transforms
import wandb

# Before running this script, first log in using command line:
# wandb login [USER_API_KEY]

def create_wandb_image(image_dict : dict):
    min_i = 0
    losses = torch.tensor([
        torch.nn.functional.mse_loss(image_dict["ab"][i], image_dict["ground_truth"][i]) for i in range(image_dict["ab"].shape[0])
    ])

    _, min_i = torch.min(losses, dim=0, keepdim=False)

    l = torchvision.transforms.Resize([112, 112])(image_dict["l"][min_i].unsqueeze(0))
    a = image_dict["ab"][min_i][0].detach()
    b = image_dict["ab"][min_i][1].detach()

    lab_image = torch.cat((l, a.unsqueeze(0), b.unsqueeze(0)), dim=0)

    exp_a = image_dict["ground_truth"][min_i][0].detach()
    exp_b = image_dict["ground_truth"][min_i][1].detach()

    exp_lab_image = torch.cat((l, exp_a.unsqueeze(0), exp_b.unsqueeze(0)), dim=0)

    image_array = [
            l,
            exp_a.unsqueeze(0),
            exp_b.unsqueeze(0),
            a.unsqueeze(0),
            b.unsqueeze(0),
            LAB2RGBConversion()(lab_image.cpu()),
            LAB2RGBConversion()(exp_lab_image.cpu())
        ]

    captions = ["L (input)", "Target A", "Target B", "Output A", "Output B", "Results in RGB", "Original"]

    return [wandb.Image(image_array[i], caption=captions[i]) for i in range(len(image_array))]


def train(
    model: LTBC2016Net,
    dataset: StaticDataset | DeeplakeDataset,
    epochs: int,
    pretrained_model_path: str | None = None,
    batch_size: int = 128,
    log_train_batch_count: int = 20,
    optimizer: str = optim.Adadelta.__name__,
    optimizer_args: dict[str, str | int | float] | None = None,
    classification_loss_influence_ratio: float = 1,
    device=torch.device('cpu'),
    use_parallel=False,
):
    model.classification_output = True
    global_classes_count = model.global_classes_count

    if use_parallel:
        model = torch.nn.DataParallel(model)

    if pretrained_model_path is not None:
        model.load_state_dict(torch.load(pretrained_model_path, map_location=device))
        model.train()

    model.to(device)

    augmentation = True
    train_loader = dataset.dataloader(type='train', pin_memory=True, shuffle=True, augmented=augmentation, batch_size=batch_size, workers_num=8)
    valid_loader = dataset.dataloader(type='valid', pin_memory=True, shuffle=False, batch_size=batch_size, workers_num=8)

    wrapped_train_loader = DataloaderDeviceWrapper(train_loader, device)
    wrapped_valid_loader = DataloaderDeviceWrapper(valid_loader, device)

    optimizer_args = optimizer_args or {}
    with wandb.init(
        project='image-colorizer',
        config={
            'architecture': 'LTBC2016Net',
            'git_commit_id': git.Repo(search_parent_directories=True).head.object.hexsha,
            'dataset': dataset.config['name'],
            'augmented' : augmentation,
            'global_classes_count': global_classes_count,
            'batch_size' : batch_size,
            'device' : device.type,
            'classification_loss_influence_ratio': classification_loss_influence_ratio,
            'optimizer': {
                'name': optimizer,
                **optimizer_args
            },
            'epochs': epochs,
            'log_train_batch_count': log_train_batch_count
        }
    ) as run:
        best_model = model
        best_model_validation_loss = float('inf')

        params = list(model.parameters())
        optimizer: optim.Optimizer = getattr(optim, optimizer)(params, **optimizer_args)

        train_data_count = len(wrapped_train_loader)
        valid_data_count = len(wrapped_valid_loader)

        for epoch in range(1, epochs + 1):
            agg_colorization_loss = 0.0  # Sum per-image
            agg_classification_loss = 0.0  # Sum per-image
            agg_combined_loss = 0.0  # Sum per-image
            agg_images = 0  # Number of aggregated image information

            best_loss_images_dict = None
            best_loss = None

            batch_data: dict[str, Tensor]
            for batch_index, batch_data in enumerate(wrapped_train_loader):
                l = batch_data['l']
                ab = batch_data['ab']
                labels = batch_data['labels']

                local_batch_size = len(l)

                optimizer.zero_grad()
                output_ab, output_classes = model.forward(l)

                colorization_loss = torch.nn.functional.mse_loss(output_ab, ab)  # Per-pixel AVG colorization loss
                classification_loss = torch.nn.functional.cross_entropy(output_classes, labels)  # Per-image AVG classification loss
                combined_loss = colorization_loss + classification_loss_influence_ratio * classification_loss  # Per-image AVG combined loss

                combined_loss.backward()
                optimizer.step()

                agg_colorization_loss += colorization_loss.item() * local_batch_size
                agg_classification_loss += classification_loss.item() * local_batch_size
                agg_combined_loss += combined_loss.item() * local_batch_size
                agg_images += local_batch_size

                if batch_index % log_train_batch_count == 0:
                    run.log({
                        'epoch': epoch,
                        'epoch_batch_index': batch_index,
                        'total_batch_index': (epoch - 1) * train_data_count + batch_index,
                        'colorization_loss': agg_colorization_loss / agg_images,
                        'classification_loss': agg_classification_loss / agg_images,
                        'combined_loss': agg_combined_loss / agg_images
                    })

                    agg_colorization_loss = 0.0  # Sum per-image
                    agg_classification_loss = 0.0  # Sum per-image
                    agg_combined_loss = 0.0  # Sum per-image
                    agg_images = 0  # Number of aggregated image information

                if best_loss == None or colorization_loss.item() < best_loss:
                    best_loss = colorization_loss.item()
                    best_loss_images_dict = { "l" : l, "ground_truth" : ab, "ab" : output_ab }

            if best_loss_images_dict is not None:
                image = create_wandb_image(best_loss_images_dict)
                wandb.log({"best_train_loss_image": image})

            with torch.no_grad():
                agg_colorization_loss = 0.0  # Sum of per-image
                agg_classification_loss = 0.0  # Sum of per-image
                agg_combined_loss = 0.0  # Sum of per-image

                best_loss_images_dict = None
                best_loss = None

                for batch_index, batch_data in enumerate(wrapped_valid_loader):
                    l = batch_data['l']
                    ab = batch_data['ab']
                    labels = batch_data['labels']

                    output_ab, output_classes = model.forward(l)

                    colorization_loss = torch.nn.functional.mse_loss(output_ab, ab, reduction='sum')  # Sum of per-image colorization loss in batch
                    classification_loss = torch.nn.functional.cross_entropy(output_classes, labels, reduction='sum')  # Sum of per-image classification loss in batch
                    combined_loss = colorization_loss + classification_loss_influence_ratio * classification_loss  # Sum of per-image combined loss in batch

                    agg_colorization_loss += colorization_loss.item()
                    agg_classification_loss += classification_loss.item()
                    agg_combined_loss += combined_loss.item()

                    if best_loss == None or colorization_loss.item() < best_loss:
                        best_loss = colorization_loss.item()
                        best_loss_images_dict = { "l" : l, "ground_truth" : ab, "ab" : output_ab }

                if best_loss_images_dict is not None:
                    image = create_wandb_image(best_loss_images_dict)
                    wandb.log({"best_valid_loss_image": image})

                agg_colorization_loss /= (valid_data_count*224*224)  # Per-pixel avg
                agg_classification_loss /= valid_data_count  # Per-image avg
                agg_combined_loss /= valid_data_count  # Per-image avg

                run.log({
                    'epoch': epoch,
                    'valid_colorization_loss': agg_colorization_loss,
                    'valid_classification_loss': agg_classification_loss,
                    'valid_combined_loss': agg_combined_loss
                })

                if agg_combined_loss < best_model_validation_loss:
                    best_model = deepcopy(model)
                    best_model_validation_loss = agg_combined_loss

        model_filename = f'{run.name}.pth'
        torch.save(best_model.state_dict(), model_filename)

        model_artifact = wandb.Artifact('best_model', type='model')
        model_artifact.add_file(model_filename, 'model.pth')
        run.log_artifact(model_artifact)

        return best_model
