import torch
import torch.nn as nn
import torch.nn.functional as f

from einops import rearrange

from common import split_lab_channels, cat_lab

# ------------------------------------------------------------
# Support functions
# ------------------------------------------------------------

##def right_pad_dims_to(x: torch.tensor, t: torch.tensor) -> torch.tensor:
##    """
##    Pads `t` with empty dimensions to the number of dimensions `x` has. If `t` does not have fewer dimensions than `x`
##        it is returned without change.
##    """
##    padding_dims = x.ndim - t.ndim
##    if padding_dims <= 0:
##        return t
##    return t.view(*t.shape, *((1,) * padding_dims))
## 
##def dynamic_threshold(img, percentile=0.8):
##    s = torch.quantile(
##        rearrange(img, 'b ... -> b (...)').abs(),
##        percentile,
##        dim=-1
##    )
##    # If threshold is less than 1, simply clamp values to [-1., 1.]
##    s.clamp_(min=1.)
##    s = right_pad_dims_to(img, s)
##    # Clamp to +/- s and divide by s to bring values back to range [-1., 1.]
##    img = img.clamp(-s, s) / s
##    return img

def get_index_from_list(vals, t, x_shape):
    """ 
    Returns a specific index t of a passed list of values vals
    while considering the batch dimension.
    """
    batch_size = t.shape[0]
    vals = vals.to(t)
    out = vals.gather(-1, t.long())
    return out.reshape(batch_size, *((1,) * (len(x_shape) - 1)))

# ------------------------------------------------------------
# Diffusion
# ------------------------------------------------------------


class Diffusion(nn.Module):
    """
    DDPM: Denoising Diffusion Probabilistic Model
    For example: https://github.com/mikonvergence/DiffusionFastForward/blob/master/notes/01-Diffusion-Theory.md
    """

    def __init__(self, T : int):
        super().__init__()

        # Number of diffusion steps
        self.T = T

        # Noise at each step is: 
        #   N(sqrt(1 - beta[i]) * x[i], beta[i])
        self.betas =  torch.linspace(0.0001, 0.02, self.T)

        # Noise from step 0 to step i can be calculated as:
        #   N(sqrt(alpha[i]) * x[0], 1 - alpha[i])
        # Where 
        #   alpha = cumulative product of (1 - beta)
        self.alphas = 1. - self.betas
        self.alphas_cumprod = torch.cumprod(self.alphas, axis=0)
        self.alphas_cumprod_prev = f.pad(self.alphas_cumprod[:-1], (1, 0), value=1.0)

        self.sqrt_recip_alphas = torch.sqrt(1.0 / self.alphas)
        self.sqrt_alphas_cumprod = torch.sqrt(self.alphas_cumprod)
        self.sqrt_one_minus_alphas_cumprod = torch.sqrt(1. - self.alphas_cumprod)

        self.posterior_variance = self.betas * (1. - self.alphas_cumprod_prev) / (1. - self.alphas_cumprod)

    def forward_diff(self, x_0, t):
        """ 

        :param x_0: Input image
        :param t: Timestamp 
        :result: Noised version of color channels to timestamp t
        """

        l, ab = split_lab_channels(x_0)

        # Samples from normal distribution N(0, 1)
        noise = torch.randn_like(ab)

        # Get parameters at a specific step
        sqrt_alphas_cumprod_t = get_index_from_list(self.sqrt_alphas_cumprod, t, ab.shape).to(x_0)
        sqrt_one_minus_alphas_cumprod_t = get_index_from_list(self.sqrt_one_minus_alphas_cumprod, t, ab.shape).to(x_0)

        # Noised version = N(sqrt(alpha[i]) * x[0], 1 - alpha[i])
        # Can be calculated as: 
        #   mean + variance * rand noise
        ab_noised = sqrt_alphas_cumprod_t * ab + sqrt_one_minus_alphas_cumprod_t * noise
        noised_img = torch.cat((l, ab_noised), dim=1)

        return (noised_img, noise)

    @torch.no_grad()
    def sample_timestamp(self, unet, encoder, x, t, ema=None, grayscale_emb=None):
        l, ab = split_lab_channels(x)

        # Get parameters at a specific step
        betas_t = get_index_from_list(self.betas.to(x), t, x.shape)
        sqrt_one_minus_alphas_cumprod_t = get_index_from_list(self.sqrt_one_minus_alphas_cumprod, t, x.shape)
        sqrt_recip_alphas_t = get_index_from_list(self.sqrt_recip_alphas, t, x.shape)
        posterior_variance_t = get_index_from_list(self.posterior_variance, t, x.shape)

        # Calculate model output
        if grayscale_emb is None:
            # Do not recalculate encoder for each diffusion step
            grayscale_emb = encoder(l)

        if ema is not None:
            with ema.average_parameters():
                predicted_noise = unet(x, t, grayscale_emb)
        else:
            predicted_noise = unet(x, t, grayscale_emb)

        # Predicted noise mean at a step i:
        #   mean = 1 / sqrt(alpha[i]) * (input[i] - beta[i] / sqrt(1 - alpha[i]) * model_out[i])
        beta_times_pred = betas_t * predicted_noise
        model_mean = sqrt_recip_alphas_t * (ab - beta_times_pred / sqrt_one_minus_alphas_cumprod_t)

        # Calculated next noising step
        if t == 0:
            res = cat_lab(l, model_mean)
        else:
            noise = torch.randn_like(ab)
            ab_t_pred = model_mean + torch.sqrt(posterior_variance_t) * noise 
            res = cat_lab(l, ab_t_pred)

        return res

