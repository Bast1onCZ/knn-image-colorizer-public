from copy import deepcopy

import git
import time
import torch.optim as optim
from torch import Tensor
from tqdm import tqdm
import torchvision
from matplotlib import pyplot as plt

from datasets.static import DataloaderDeviceWrapper, StaticDataset
from datasets.deeplake import DeeplakeDataset
from torch.optim.lr_scheduler import ExponentialLR

from kornia.filters import sobel

from src.model.our_diffusion.network import Network
from src.model.our_diffusion.diffusion import Diffusion
from src.model.our_diffusion.common import split_lab_channels, cat_lab
import torch
import wandb
from src.model.our_diffusion.common import show_lab_image, lab_to_rgb, lab_to_pil
from src.model.our_diffusion.common import sample_plot_image

# Before running this script, first log in using command line:
# wandb login [USER_API_KEY]

# TODO: cuda device

class Trainer:
    def __init__(
        self,
        dataset : StaticDataset | DeeplakeDataset,
        device=torch.device('cpu'),

        epochs: int = 50,
        batch_size: int = 8, ### 128,
        grad_accumulation : int = 1,
        optimizer: str = optim.AdamW.__name__,
        optimizer_args: dict[str, str | int | float] | None = None,
        lr_decay : float = 0.8,
        diffusion_steps : int = 350,
        edge_weight : float = 5.0,

        use_parallel=False,
        log_train_batch_count: int = 20,
        log_model_epoch_count: int = 1,
        log_train_img_time: int = 3600, # Seconds
        log_model_train_time: int = 3600, # Seconds
    ):
        self.epochs = epochs
        self.batch_size = batch_size
        self.grad_accumulation = grad_accumulation
        self.optimizer = optimizer
        self.optimizer_args = optimizer_args or {}
        self.lr_decay = lr_decay
        self.diffusion_steps = diffusion_steps
        self.edge_weight = edge_weight
        self.log_train_batch_count = log_train_batch_count
        self.log_model_epoch_count = log_model_epoch_count
        self.log_train_img_time = log_train_img_time
        self.log_model_train_time = log_model_train_time

        print("Init dataloaders")
        train_loader = dataset.dataloader(type='train', pin_memory=True, shuffle=True, batch_size=batch_size, workers_num=8)
        valid_loader = dataset.dataloader(type='valid', pin_memory=True, shuffle=False, batch_size=batch_size, workers_num=8)
        
        self.wrapped_train_loader = DataloaderDeviceWrapper(train_loader, device)
        self.wrapped_valid_loader = DataloaderDeviceWrapper(valid_loader, device)

        #self.wrapped_train_loader = dataset[0]
        #self.wrapped_valid_loader = dataset[1]

        print("Init wandb logger")
        self.logger = wandb.init(
            project='image-colorizer',
            config={
                'architecture': 'OurDiffusion',
                'git_commit_id': git.Repo(search_parent_directories=True).head.object.hexsha,
                'device' : device.type,
                'dataset': dataset.config['name'],
                #'dataset': 'celeb',

                'epochs': epochs,
                'batch_size' : batch_size,
                'grad_accumulation': grad_accumulation,
                'optimizer': {
                    'name': optimizer,
                    **self.optimizer_args
                },
                'lr_decay': lr_decay,
                'diffusion_steps': diffusion_steps,
                'edge_weight': edge_weight,
                
                'unet_channels': self.model.unet.INNER_CHANNELS,
            })
       
        print("Init model")
        self.model = Network(device=device)
        if use_parallel:
            self.model = torch.nn.DataParallel(self.model)
        self.model.to(device)

        print("Wandb watch model")
        self.logger.watch(self.model, log_freq=self.log_train_batch_count, log_graph=True)

        print("Init diffusion")
        self.diffusion = Diffusion(diffusion_steps)

       
    def train(self):
        best_model = self.model
        best_model_validation_loss = float('inf')

        params = list(self.model.parameters())
        optimizer: optim.Optimizer = getattr(optim, self.optimizer)(params, **self.optimizer_args)
        scheduler = ExponentialLR(optimizer, gamma=self.lr_decay)

        train_data_count = len(self.wrapped_train_loader)
        valid_data_count = len(self.wrapped_valid_loader)

        start = time.time()

        for epoch in range(1, self.epochs + 1):
            batch_data: dict[str, Tensor]
            curr_loss = 0.0
            edge_loss = 0.0
            curr_loss_cnt = 0

            print(f"Epoch: {epoch} ----------------")
            self.log_img()
            
            last_img_time = time.time()
            last_model_time = time.time()

            for batch_index, batch_data in enumerate(self.wrapped_train_loader):
                c_loss, e_loss = self.step(batch_index, batch_data, optimizer)
                curr_loss += c_loss
                edge_loss += e_loss
                curr_loss_cnt += self.batch_size

                if batch_index % self.log_train_batch_count == 0:
                    #print(f"    Train loss: {curr_loss}")
                    self.logger.log({
                        'epoch': epoch,
                        'epoch_batch_index': batch_index,
                        'total_batch_index': epoch * train_data_count + batch_index,
                        'train_loss': curr_loss / curr_loss_cnt,
                        'train_edge_loss': edge_loss / curr_loss_cnt,
                    })

                    curr_loss = 0.0
                    edge_loss = 0.0
                    curr_loss_cnt = 0
            
                if time.time() - last_img_time > self.log_train_img_time:
                    self.log_img(batch_data)
                    last_img_time = time.time()

                if time.time() - last_model_time > self.log_model_train_time:
                    self.save_model(best_model)
                    last_model_time = time.time()
                
            print(f"    Train epoch time: {time.time() - start} s")
            print(f"    Run validation")
            with torch.no_grad():
                curr_loss = 0
                edge_loss = 0

                for batch_index, batch_data in enumerate(self.wrapped_valid_loader):
                    c_loss, e_loss = self.step(batch_index, batch_data, optimizer, train=False)
                    curr_loss += c_loss
                    edge_loss += e_loss
                    
                    if batch_index == 0:
                        self.log_img(batch_data)

                self.logger.log({
                    'epoch': epoch,
                    'valid_loss': curr_loss,
                    'valid_edge_loss': edge_loss,
                })

                if curr_loss < best_model_validation_loss:
                    best_model = deepcopy(self.model)
                    best_model_validation_loss = curr_loss
            
            print(f"    Validation loss: {curr_loss}")

            if epoch % self.log_model_epoch_count == 0:
                self.save_model(best_model)
                last_mode_time = time.time()

            # Learning rate decay
            scheduler.step()

        self.logger.finish()

        return best_model, best_model_validation_loss

    def save_model(self, best_model):
        print("    Saving model")
        model_filename = f'{self.logger.name}.pth'
        torch.save(best_model.state_dict(), model_filename)
        
        model_artifact = wandb.Artifact('best_model', type='model')
        model_artifact.add_file(model_filename, 'model.pth')
        self.logger.log_artifact(model_artifact)

    def get_batch_prediction(self, x_0, x_l):
        """
        Samples a timestamp from range [0, T]
        Adds noise to images x_0 to get x_t (x_0 with color channels noised)
        Returns:
        - The model's prediction of the noise, 
        - The real noise applied to the color channels by the forward diffusion process
        """

        t = torch.randint(0, self.diffusion_steps, (x_0.shape[0],)).to(x_0)
        x_noised, noise = self.diffusion.forward_diff(x_0, t)
        prediction = self.model.forward(x_noised, t, x_l)

        return (prediction, noise)
    
    def step(self, batch_index, batch_data, optimizer, train=True):
        x_l = batch_data['l']
        x_ab = batch_data['ab']
        
        x_l = x_l[:, None, :, :]
        x_0 = cat_lab(x_l, x_ab)

        x_0 = x_0.to(self.model.device)
        x_l = x_l.to(self.model.device)

        noise_prediction, noise = self.get_batch_prediction(x_0, x_l)
        loss = torch.nn.functional.mse_loss(noise_prediction, noise)
        
        noise_pred_edges = sobel(noise_prediction)
        noise_edges = sobel(noise)
        edge_loss = torch.nn.functional.mse_loss(noise_pred_edges, noise_edges)

        loss += edge_loss * self.edge_weight

        if train:
            loss.backward()
            if (batch_index + 1) % self.grad_accumulation == 0:
                # Gradient accumulation
                # Acts as large batch size while keeping low memory usage
                optimizer.step()
                optimizer.zero_grad()

        return loss.item(), edge_loss.item()

    def log_img(self, batch=None):
        print("    Log image")
        if batch is None:
            x = next(iter(self.wrapped_valid_loader))
        else:
            x = batch

        x_l = x['l']
        x_ab = x['ab']
        labels = x['labels']
        
        x_l = x_l[:, None, :, :]
        x_0 = cat_lab(x_l, x_ab)

        if batch is None:
            x = x_0.to(self.model.device)
        else:
            x = x_0.to(batch['l'].device)
        
        #self.sample_plot_image(x, log=True)

        #for i in range(x.shape[0]):
        #    img = x[i].unsqueeze(0)
        sample_plot_image(x, self.diffusion_steps, self.model, self.diffusion, 
                        show=False, save_paths=None, logger=self.logger)

        print("    Log image done")
