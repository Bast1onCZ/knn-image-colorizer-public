import sys

sys.path.append('./')

import torch
import multiprocessing as mp
from argparse import ArgumentParser
from datasets.deeplake import DeeplakeDataset
##from datasets.static import StaticDataset
from src.model.our_diffusion.trainer import Trainer
import torch.optim as optim

import numpy as np
import glob
import random
from PIL import Image, ImageChops
from common import split_lab_channels
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms
from skimage.color import rgb2lab

### FOR CELEBA DATASET:
###class ColorizationDataset(Dataset):
###    def __init__(self, paths, split='train', config=None):
###        size = config["img_size"]
###        self.resize = transforms.Resize((size, size), Image.BICUBIC)
###        if split == 'train':
###            self.transforms = transforms.Compose([
###                transforms.RandomHorizontalFlip(),
###                transforms.ColorJitter(brightness=0.3,
###                                       contrast=0.1,
###                                       saturation=(1., 2.),
###                                       hue=0.05),
###                self.resize
###            ])
###        elif split == 'val':
###            self.transforms = self.resize
###        self.paths = paths
### 
###    def tensor_to_lab(self, base_img_tens):
###        base_img = np.array(base_img_tens)
###        img_lab = rgb2lab(base_img).astype(
###            "float32")  # Converting RGB to L*a*b
###        img_lab = transforms.ToTensor()(img_lab)
###        L = img_lab[[0], ...] / 50. - 1.  # Between -1 and 1
###        ab = img_lab[[1, 2], ...] / 110.  # Between -1 and 1
###        return torch.cat((L, ab), dim=0)
### 
###    def get_lab_from_path(self, path):
###        img = Image.open(path).convert("RGB")
###        img = self.transforms(img)
###        return self.tensor_to_lab(img)
### 
###    def get_rgb(self, idx=0):
###        img = Image.open(self.paths[idx]).convert("RGB")
###        img = self.transforms(img)
###        img = np.array(img)
###        return (img)
### 
###    def get_grayscale(self, idx=0):
###        img = Image.open(self.paths[idx]).convert("L")
###        img = self.resize(img)
###        img = np.array(img)
###        return (img)
### 
###    def get_lab_grayscale(self, idx=0):
###        img = self.get_lab_from_path(self.paths[idx])
###        l, _ = split_lab_channels(img.unsqueeze(0))
###        return torch.cat((l, *[torch.zeros_like(l)] * 2), dim=1)
### 
###    def __getitem__(self, idx):
###        return self.get_lab_from_path(self.paths[idx])
### 
###    def __len__(self):
###        return len(self.paths)
### 
###def make_datasets(path, config, limit=None):
###    img_paths = glob.glob(path + "/*")
###    if limit:
###        img_paths = random.sample(img_paths, limit)
###    n_imgs = len(img_paths)
###    train_split = img_paths[:int(n_imgs * .9)]
###    val_split = img_paths[int(n_imgs * .9):]
### 
###    train_dataset = ColorizationDataset(
###        train_split, split="train", config=config)
###    val_dataset = ColorizationDataset(val_split, split="val", config=config)
###    print(f"Train size: {len(train_split)}")
###    print(f"Val size: {len(val_split)}")
###    return train_dataset, val_dataset
### 
### 
###def make_dataloaders(path, config, num_workers=2, shuffle=True, limit=None):
###    train_dataset, val_dataset = make_datasets(path, config, limit=limit)
###    train_dl = DataLoader(train_dataset,
###                          batch_size=config["batch_size"],
###                          num_workers=num_workers,
###                          pin_memory=config["pin_memory"],
###                          persistent_workers=True,
###                          shuffle=shuffle)
###    val_dl = DataLoader(val_dataset,
###                        batch_size=config["batch_size"],
###                        num_workers=num_workers,
###                        pin_memory=config["pin_memory"],
###                        persistent_workers=True,
###                        shuffle=shuffle)
###    return train_dl, val_dl
### 



if __name__ == '__main__':
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    print(f'Running on device: {device}')
    print(f"CUDA-driver version: {torch.version.cuda}")

    mp.set_start_method("spawn")
    dataset_conf = DeeplakeDataset.DATASETS["places205"]
    #dataset_conf = DeeplakeDataset.DATASETS["places205-reduced"]
    #dataset_conf = DeeplakeDataset.DATASETS["places205-mini"]
    dataset_conf['ab_size'] = [224, 224]
    dataset_conf['l_size']  = [224, 224]
    dataset = DeeplakeDataset(dataset_conf)
    
    ######torch.set_num_threads(2)
    ##parser = ArgumentParser()
    ##parser.add_argument("--dataset", default="src/model/diffusion/img_align_celeba", help="Path to unzipped dataset (see readme for download info)")
    ##args = parser.parse_args()
    ##config = {
    ##    'device': 'auto',
    ##    'pin_memory': True,
    ##    'T': 350,
    ##    'lr': 1.0e-06,
    ##    'loss_fn': 'l2',
    ##    'batch_size': 8,
    ##    'accumulate_grad_batches': 2,
    ##    'img_size': 64,
    ##    'sample': True,
    ##    'should_log': True,
    ##    'epochs': 14,
    ##    'using_cond': True,
    ##    'display_every': 350,
    ##    'dynamic_threshold': False,
    ##    'train_autoenc': False,
    ##    'enc_loss_coeff': 1.1,
    ##}
    ##train_dl, val_dl = make_dataloaders(args.dataset, config, num_workers=2) #35_000)

    print("Init Trainer ...")
    trainer = Trainer(
        device = device,
        dataset = dataset, #(train_dl, val_dl),

        epochs = 50,
        batch_size = 4,
        grad_accumulation = 32, #4
        optimizer = optim.AdamW.__name__,
        optimizer_args = {'lr': 3.0e-06},
        lr_decay = 0.5,
        diffusion_steps = 500,
        edge_weight=0.0,

        use_parallel=False,
        log_train_batch_count=10,
        log_model_epoch_count=1,
        log_train_img_time=30*60, # Seconds
        log_model_train_time=2*3600,
    )
    
    print("Starting training")
    trainer.train()
    print("Training finished")
