import sys
import os

sys.path.append('./')

from datasets.utils import create_path
from datasets.deeplake import DeeplakeDataset

def collate_f(batch):
    """Collate operation for places205 dataset."""

    images = [x["images"] for x in batch]

    return {
        "images" : images,
        "labels" : [x["labels"] for x in batch],
        "index" : [x["index"] for x in batch]
    }


if len(sys.argv) < 4:
    print("USAGE: python get_img.py <dset_name> <target_dir_path> <number_of_samples>")
    exit(1)

dataset = DeeplakeDataset(DeeplakeDataset.DATASETS[sys.argv[1]])
target_dir_path = sys.argv[2]
samples_num = int(sys.argv[3])

dataset_subsets = [
        [dataset.train_dataset, "train"],
        [dataset.valid_dataset, "valid"],
        [dataset.test_dataset, "test"]
    ]


for d in dataset_subsets:
    dataloader = d[0].pytorch(
        num_workers=4,
        batch_size=32,
        shuffle=True,
        tensors=dataset.config["tensors"],
        collate_fn=collate_f,
        #transform=self.config["transform"],
        decode_method={"images" : "pil"},
        buffer_size=8,
        pin_memory=True
    )

    dir_path = os.path.join(target_dir_path, d[1])

    create_path(dir_path)

    cnt = 0
    for b in dataloader:
        for i in range(len(b["images"])):
            image = b["images"][i]
            index = b["index"][i][0]
            label = b["labels"][i][0]

            image.save(os.path.join(dir_path, f"{index}-{label}.jpg"))

            cnt += 1
            if cnt >= samples_num:
                break

        if cnt >= samples_num:
            break



