import sys

sys.path.append('./')

import os
import glob
import git
import time
import torch
from torch.profiler import profile, record_function, ProfilerActivity
import torchvision
from matplotlib import pyplot as plt
from argparse import ArgumentParser
from PIL import Image 
import torchvision.transforms as transforms 
import numpy as np
from kornia.filters import sobel
import wandb
import multiprocessing as mp
from pathlib import Path

from src.model.our_diffusion.common import sample_plot_image
from src.model.our_diffusion.network import Network
from src.model.our_diffusion.diffusion import Diffusion
from datasets.utils import RGB2LABConversion


def download_model(artifactID, path='image-colorizer/image-colorizer/best_model:v'):
    run = wandb.init()
    artifact = run.use_artifact(f'{path}{artifactID}', type='model')
    artifact_dir = artifact.download()

    print(artifact_dir)
    return f'artifacts/best_model:v{artifactID}/model.pth'

class Inference:
    def __init__ (self, device, diffusion_steps, img_size=[224, 224]):
        self.device = device
        self.diffusion_steps = diffusion_steps
        self.img_size = img_size

        self.model = Network(device=device, print_model=False)
        self.model.to(device)

        self.diffusion = Diffusion(diffusion_steps)

    def load_model(self, model_path):
        self.model.load_state_dict(torch.load(model_path, map_location=self.device))
        self.model.eval()

    def plot(self, batch_size, img_path, short_path, res_path, show=True, logger=None):
        if not os.path.exists(res_path):
            os.makedirs(res_path)

        images = []
        for img in img_path:
            image = Image.open(img).convert("RGB")
            #print(f'Input image size: {image.size}')

            transform = transforms.Compose([
                torchvision.transforms.ToTensor(),
                torchvision.transforms.RandomCrop(self.img_size),
                RGB2LABConversion(),
            ])

            image = transform(image) 
            image.to(self.model.device)
            image = image.unsqueeze(0)

            #edges = sobel(image)
            #plt.imshow(edges[0].permute(1, 2, 0).cpu().detach().numpy())
            #plt.show()

            images.append(image)

        images = torch.cat(images)
        save_paths = []
        for i in range(images.shape[0]):
            img = images[i].unsqueeze(0)
            img = img.to(self.device)

            img_name = short_path[i].split('.')[0]
            save_path = res_path + f'{img_name}'
            parent = Path(save_path).parent
            if not os.path.exists(parent):
                os.makedirs(parent)
            save_paths.append(save_path)

        batched = images.unfold(0, batch_size, batch_size)
        batched = batched.permute(0, 4, 1, 2, 3)
        paths_batched = [save_paths[i:i+batch_size] for i in range(0, len(save_paths), batch_size)]
        for i, batch in enumerate(batched):
            batch = batch.to(self.device)
            sample_plot_image(batch, self.diffusion_steps, self.model, self.diffusion, 
                            show=show, save_paths=paths_batched[i], logger=logger)

MODEL_ID = 24 #52
DIFFUSION_STEPS = 500
BATCH_SIZE = 3
DOWNLOAD_MODEL = False
WANDB_LOG = False
ROOT_OUT = ''
ROOT_IN = 'test_imgs/'

# 2 = on full places (almost)
# 24 = reduced
#MODEL_ID = 24 #57
#DIFFUSION_STEPS = 500
#BATCH_SIZE = 8
#DOWNLOAD_MODEL = True
#WANDB_LOG = True
#ROOT_OUT = '/storage/brno2/home/xnevrk03/knn-image-colorizer-public/'
##ROOT_IN = '/storage/brno2/home/xnevrk03/data_knn/places205-reduced-samples'
#ROOT_IN = '/storage/brno2/home/xnevrk03/data_knn/places205-samples'

PRIOR = [
    '1085534-87' , '145-0',       '215160-16',   '547087-44', '644413-52',
    '1329821-109', '1902643-155', '2361305-192', '547985-44', '732423-58',
    '1414353-116', '1975767-162', '455534-36',   '564972-46', '916274-73',
]

IN_FOLDER  = ROOT_IN
OUT_FOLDER = f'{ROOT_OUT}res_imgs/'

if __name__ == '__main__':
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    print(f'Running on device: {device}')

    if WANDB_LOG:
        logger = wandb.init(
            project='image-colorizer',
            config={
                'architecture': 'OurDiffusionInference',
                'git_commit_id': git.Repo(search_parent_directories=True).head.object.hexsha,
                'device' : device.type,
                'model_id': MODEL_ID,
                'diffusion_steps': DIFFUSION_STEPS,
            })
    else:
        logger = None

    if DOWNLOAD_MODEL:
        model_path = download_model(MODEL_ID)
    else:
        model_path = f'artifacts/best_model:v{MODEL_ID}/model.pth'

    inference = Inference(device, diffusion_steps=DIFFUSION_STEPS)
    inference.load_model(model_path)

    #with profile(activities=[ProfilerActivity.CPU], profile_memory=True, record_shapes=True) as prof:
    #    with record_function("model_inference"):

    if IN_FOLDER is not None:
        imgs_path = list(glob.glob(f'{IN_FOLDER}/**', recursive=True))
        imgs_path = [img for img in imgs_path if img.split('.')[-1] in ['jpg', 'jpeg', 'png']]

        # Prioritize some images
        prior = [img for img in imgs_path if Path(img).stem in PRIOR]
        imgs_path = [img for img in imgs_path if Path(img).stem not in PRIOR]
        imgs_path = prior + imgs_path

        short_path = [x[len(IN_FOLDER):] for x in imgs_path]

        start = time.time()
        inference.plot(BATCH_SIZE, imgs_path, short_path, OUT_FOLDER, show=True, logger=logger)
        end = time.time()

        print(f"Time: {end - start} s\n\n")
    
    #print(prof.key_averages().table(sort_by="cpu_time_total", row_limit=10))

