#!/bin/bash

if test -d .git; then
    echo ".git FOUND"
else
    echo ".git NOT FOUND"
fi

if test -d .git; then
    echo "ENV FOUND"
else
    echo "CREATING THE NEW VIRTUAL ENV"
    pip install virtualenv
    python3 -m virtualenv env
fi

echo "CUDA RUNTIME VERSION:"
nvcc --version

. ./env/bin/activate
pip install -r requirements.txt

if [ -z $WANDB_API_KEY ]; then
    echo "WAND_KEY NOT DEFINED running wandb loging without it"
    python -m wandb login
else
    python -m wandb login $WANDB_API_KEY
fi

python -m wandb online # python -m wandb offline

python3 $RUN_SCRIPT_PATH
