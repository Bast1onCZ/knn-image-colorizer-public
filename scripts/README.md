# Meta-centrum

## Documentation

- https://docs.metacentrum.cz/computing/

## Login

- `ssh xnevrk03@perian.grid.cesnet.cz`
    - SSH klíč: https://wiki.metacentrum.cz/wiki/Setting_the_SSH_key
    - TODO
- `scp x xnevrk03@perian.grid.cesnet.cz:~`

## Jobs

- From script: `qsub run.sh`
- From CLI: `qsub -q gpu -l select=1:ncpus=1:ngpus=1:scratch_local=10gb -l walltime=2:0:0 cnn_test.py`
- Interactive:
    - `qsub -I -q gpu -l select=1:ncpus=1:scratch_local=10gb:ngpus=1:gpu_cap=cuda60 -l walltime=2:0:0 -m ae`
- Status:
    - `qstat -xf -u xnevrk03`
    - Ze všech serverů: `qstat -xf -u xnevrk03 @meta-pbs.metacentrum.cz @cerit-pbs.cerit-sc.cz @elixir-pbs.elixir-czech.cz`
        - Protože script se přesune na jiný server
    - Přes web: https://metavo.metacentrum.cz/pbsmon2/user/xnevrk03

## Modules

- `module add pytorch`
- https://docs.metacentrum.cz/software/install-software/
- `/storage/plzen1/home/xnevrk03`

## Output of running jobs

Jak se podívat na aktuální výstup běžícího scriptu (celkem pain, ale jde to):

Although the input and temporary files for calculation lie in $SCRACHDIR, the standard output (STDOUT) and standard error output (STDERR) are elsewhere.

- find on which host the job runs by `qstat -f job_ID | grep exec_host2`
- ssh to this host
- on the host, navigate to `/var/spool/pbs/spool/` directory and examine the files
    - $PBS_JOBID.OU for STDOUT, e.g. 13031539.meta-pbs.metacentrum.cz.OU
    - $PBS_JOBID.ER for STDERR, e.g. 13031539.meta-pbs.metacentrum.cz.ER
- To watch a file continuously, you can also use a command tail -f
- For example:

```
(BULLSEYE)user123@tarkil:~$ qstat -f 13031539.meta-pbs.metacentrum.cz | grep exec_host2
exec_host2 = zenon41.cerit-sc.cz:15002/12
(BULLSEYE)user123@tarkil:~$ ssh zenon41.cerit-sc.cz
user123@zenon41.cerit-sc.cz:/var/spool/pbs/spool$ tail -f 13031539.meta-pbs.metacentrum.cz.OU
```

## Job script (OLD)

```
#!/bin/bash
#PBS -N knn_test
#PBS -q gpu
#PBS -l select=1:ncpus=1:mem=16gb:scratch_local=10gb:ngpus=1:gpu_cap=cuda60
#PBS -l walltime=2:00:00
#PBS -m ae

DATADIR=/storage/brno2/home/xnevrk03/knn_test
SCRIPT=$DATADIR/knn_test.py

# Scratch directory = temporary outputs
# test if scratch directory is set
test -n "$SCRATCHDIR" || { echo >&2 "Variable SCRATCHDIR is not set!"; exit 1; }

# Optional: copy files to scratch directory
#cp $DATADIR/h2o.com  $SCRATCHDIR || { echo >&2 "Error while copying input file(s)!"; exit 2; }

cd $SCRATCHDIR

# Run in Docker image with pytorch
singularity run --nv /cvmfs/singularity.metacentrum.cz/NGC/PyTorch\:20.09-py3.SIF  python3 $SCRIPT

# clean the SCRATCH directory
clean_scratch
```

