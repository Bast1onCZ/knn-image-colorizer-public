import torch
import torch.nn as nn
import torch.nn.functional as f

from common_modules import SinusoidalPosEmb, Residual, PreNorm, LinearAttention, Attention, Downsample, Upsample, ResnetBlock

# ------------------------------------------------------------
# Main U-NET 
# ------------------------------------------------------------

class UNet(nn.Module):
    """
    Diffusion UNet
    Input: Lightness channel + A, B channels with random noise
    Output: A, B channels representing colorized image
    """

    def __init__(self, in_channels : int = 3, out_channels : int = 2):
        super().__init__()

        # ------------------------------------------------------------
        # Parameters
        # ------------------------------------------------------------

        self.RESNET_GROUPS = 8
        self.DROPOUT = 0.3
        self.INNER_CHANNELS = [128, 128, 128 * 2, 128 * 3, 128 * 3]

        # Get pairs (in_ch, out_ch)
        init_ch = self.INNER_CHANNELS[0]
        inner_ch_io = list(zip(self.INNER_CHANNELS[:-1], self.INNER_CHANNELS[1:]))
        mid_ch = self.INNER_CHANNELS[-1]

        # ------------------------------------------------------------
        # Time MLP
        # ------------------------------------------------------------

        time_ch = init_ch * 4
        self.time_mlp = nn.Sequential(
            SinusoidalPosEmb(init_ch),
            nn.Linear(init_ch, time_ch),
            # Gaussian Error Linear Units
            nn.GELU(),
            nn.Linear(time_ch, time_ch)
        )
        
        # ------------------------------------------------------------
        # Layers
        # ------------------------------------------------------------

        self.init_conv = nn.Conv2d(in_channels, init_ch, 7, padding = 3)

        self.downs = nn.ModuleList([])
        for i, (in_d, out_d) in enumerate(inner_ch_io):
            is_last = i >= (len(inner_ch_io) - 1)

            self.downs.append(nn.ModuleList([
                ResnetBlock(in_d, in_d, time_emb_dim=time_ch, groups=self.RESNET_GROUPS),
                ResnetBlock(in_d, in_d, time_emb_dim=time_ch, groups=self.RESNET_GROUPS),
                Residual(PreNorm(in_d, LinearAttention(in_d))),
                Downsample(in_d * 2, out_d, dropout=self.DROPOUT) if not is_last else nn.Conv2d(in_d * 2, out_d, 3, padding = 1)
            ]))

        self.mid_block1 = ResnetBlock(mid_ch, mid_ch, time_emb_dim=time_ch, groups=self.RESNET_GROUPS)
        self.mid_attn = Residual(PreNorm(mid_ch, Attention(mid_ch)))
        self.mid_block2 = ResnetBlock(mid_ch, mid_ch, time_emb_dim=time_ch, groups=self.RESNET_GROUPS)

        self.ups = nn.ModuleList([])
        for i, (in_d, out_d) in enumerate(reversed(inner_ch_io)):
            is_last = i >= (len(inner_ch_io) - 1)

            self.ups.append(nn.ModuleList([
                ResnetBlock(in_d + out_d, out_d, time_emb_dim=time_ch, groups=self.RESNET_GROUPS),
                ResnetBlock(in_d + out_d, out_d, time_emb_dim=time_ch, groups=self.RESNET_GROUPS),
                Residual(PreNorm(out_d, LinearAttention(out_d))),
                Upsample(out_d, in_d, dropout=self.DROPOUT) if not is_last else  nn.Conv2d(out_d, in_d, 3, padding = 1)
            ]))


        self.fin_resnet_block = ResnetBlock(init_ch * 2, init_ch, time_emb_dim=time_ch, groups=self.RESNET_GROUPS)
        self.final_conv = nn.Conv2d(init_ch, out_channels, 1)

    # ------------------------------------------------------------
    # Forward
    # ------------------------------------------------------------

    def forward(self, x : torch.Tensor, time : torch.Tensor, greyscale_embs : torch.Tensor = None):
        x = self.init_conv(x)
        r = x.clone()

        t = self.time_mlp(time)

        h = []

        for i, (block1, block2, attn, downsample) in enumerate(self.downs):
            x = block1(x, t)
            h.append(x)

            x = block2(x, t)
            x = attn(x)
            h.append(x)
            x = torch.cat((x, greyscale_embs[i]), dim = 1)
            x = downsample(x)

        x = self.mid_block1(x, t)
        x = self.mid_attn(x)
        x = self.mid_block2(x, t)

        for block1, block2, attn, upsample in self.ups:
            x = torch.cat((x, h.pop()), dim = 1)
            x = block1(x, t)

            x = torch.cat((x, h.pop()), dim = 1)
            x = block2(x, t)
            x = attn(x)

            x = upsample(x)

        x = torch.cat((x, r), dim = 1)
        x = self.fin_resnet_block(x, t)
        x = self.final_conv(x)

        return x
