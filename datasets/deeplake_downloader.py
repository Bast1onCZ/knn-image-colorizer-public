import deeplake
import datasets.utils
import torch.utils.data
import os
import zipfile
import io
from PIL import Image


class DeeplakeDownloader():
    """Downloader for deeplake datasets."""

    DATASETS = {
        "places205-debug" : {
            "target_data_path" : "deeplake/places205-debug/",
            "target_labels_path" : "deeplake/places205-debug/",
            "url" : "hub://activeloop/places205",
            "tensors" : ["images", "labels"],
            "collate" : datasets.utils.places205_downloader_collate,
        },
        "places205" : {
            "target_data_path" : "deeplake/places205/images",
            "target_labels_path" : "deeplake/places205",
            "url" : "hub://activeloop/places205",
            "tensors" : ["images", "labels"],
            "collate" : datasets.utils.places205_downloader_collate,
        }
    }

    def __init__(self, dataset_config : dict) -> None: # Seed is used only when the max size is notne
        self.config = dataset_config
        self.dataset = None
        self.dataset_subset = None

    def load(self, max_size = None, seed : int = 1):
        self.dataset = deeplake.load(
            self.config["url"],
            read_only=True,
            verbose=True,
            access_method="stream"
        )

        generator = torch.Generator().manual_seed(seed)

        self.dataset_subset = None
        if max_size is not None and max_size < len(self.dataset):
            self.dataset_subset, _ = torch.utils.data.random_split(
                self.dataset,
                [max_size, len(self.dataset) - max_size],
                generator
            )

        length = len(self.dataset_subset if self.dataset_subset is not None else self.dataset)
        print(f"{length} samples will be downloaded to {os.path.dirname(os.path.abspath(__file__))}")

    def _get_detaloader(self, workers : int = 2, batch_size : int = 32):
        dataset = self.dataset
        if self.dataset_subset is not None:
            dataset = self.dataset[self.dataset_subset.indices]

        return dataset.pytorch(
            num_workers=workers,
            batch_size=batch_size,
            progressbar=True,
            shuffle=False,
            tensors= self.config["tensors"],
            decode_method={"images" : "pil"},
            collate_fn=self.config["collate"]
        )

    def get(self, workers : int = 2, batch_size : int = 32):
        cur_dir = os.path.dirname(os.path.abspath(__file__))
        labels_dir = os.path.join(cur_dir, self.config["target_labels_path"])
        images_dir = os.path.join(cur_dir, self.config["target_data_path"])

        datasets.utils.create_path(os.path.join(cur_dir, self.config["target_labels_path"]))
        datasets.utils.create_path(os.path.join(cur_dir, self.config["target_data_path"]))

        dataloader = self._get_detaloader(workers, batch_size)

        csv_f = open(f"{labels_dir}/labels.csv", "a")
        zip_f = zipfile.ZipFile(f"{images_dir}/images.zip", "a", compression=zipfile.ZIP_DEFLATED)

        for data in dataloader:
            csv = []
            for i in range(len(data["images"])):
                file_name = str(data["index"][i][0]) + ".jpg"
                if not file_name in zip_f.namelist():
                    buffer = io.BytesIO()
                    data["images"][i].save(buffer, "JPEG")
                    zip_f.writestr(file_name, buffer.getvalue())
                    csv.append(f"{file_name}, {data['labels'][i][0]}\n")

            csv_f.writelines(csv)

        csv_f.close()
        zip_f.close()

