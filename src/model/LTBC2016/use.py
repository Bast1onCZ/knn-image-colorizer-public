import sys

sys.path.append('./')

import torch
import os
import torchvision
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from PIL import Image
from src.model.LTBC2016.LTBC2016Net import LTBC2016Net
from datasets.utils import LAB2RGBConversion, RGB2LABConversion, rgb2gray
from skimage.color import rgb2hsv


def colorfullness(l, a, b):
    n = l.numel()
    return ((torch.sqrt(a**2 + b**2)/torch.sqrt(a**2 + b**2 + l**2)).sum()/n) * 100

if len(sys.argv) <= 2:
    print("USAGE: use.py <pth-file> <image>")
    sys.exit(1)

matplotlib.rcParams.update({'font.size': 8})


def grayscale2l(g):
    return (g - 0.5) * 2.0

def l2grayscale(l):
    return (l / 2.0) + 0.5

pth_path = sys.argv[1]
image_path = sys.argv[2]


# Model setup
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

model = LTBC2016Net(global_classes_count=245)
model.load_state_dict(torch.load(pth_path, map_location=device))

print("Model has: ", (sum(p.numel() for p in model.parameters() if p.requires_grad)), " trainable params")

model.eval()


# Input data
rgb_input_pil_image = Image.open(image_path)

gray_input_pil_image = rgb2gray(rgb_input_pil_image)
gray_input_tensor = torchvision.transforms.ToTensor()(gray_input_pil_image)

resized_gray_image_tensor = torchvision.transforms.Resize([224, 224])(gray_input_tensor)
shrinked_gray_image_tensor = torchvision.transforms.Resize([112, 112])(gray_input_tensor)



# Validation of RGB2LAB and LAB2RGB conversion and visualiation of the expected results
rgb_input_tensor = torchvision.transforms.ToTensor()(rgb_input_pil_image)

shrinked_rgb_input_tensor = torchvision.transforms.Resize([112, 112])(rgb_input_tensor)
valid_lab_tensor = RGB2LABConversion()(shrinked_rgb_input_tensor)
valid_lab_tensor_224x224 = RGB2LABConversion()(torchvision.transforms.Resize([224, 224])(rgb_input_tensor))

l = valid_lab_tensor[0].detach()
exp_a = valid_lab_tensor[1].detach()
exp_b = valid_lab_tensor[2].detach()

exp_ab = torch.stack([torch.cat((exp_a.unsqueeze(0), exp_b.unsqueeze(0)), dim=0)])


lab_image = torch.cat((grayscale2l(shrinked_gray_image_tensor), exp_a.unsqueeze(0), exp_b.unsqueeze(0)), dim=0)


fig, ax = plt.subplots(1, 8, figsize=(15, 5))


# Expected RGB (created with LAB2RGB conversion)
reconstr_image = LAB2RGBConversion()(lab_image)
reconstr_pil_image = torchvision.transforms.ToPILImage()(reconstr_image)
ax[4].imshow(reconstr_pil_image)
ax[4].set_title('Expected')

# L and B channels
ax[5].imshow(exp_a.detach(), cmap='RdGy_r')
ax[5].set_title('Expected A')

ax[6].imshow(exp_b.detach(), cmap='RdBu_r')
ax[6].set_title('Expected B')

# Original image to be sure, that conversion works
ax[7].imshow(torchvision.transforms.Resize([112, 112])(rgb_input_pil_image))
ax[7].set_title('Orig')


input_tensor = grayscale2l(resized_gray_image_tensor)
#input_tensor = torch.stack([valid_lab_tensor_224x224[0].detach()]) # Uncomment to use L channel instead of grayscale channel

# Inference
ab, classes = model.forward(input_tensor)

print(f"Classification result: {classes}")

print(f"The most probable class: {torch.argmax(classes)} (confidence {classes[0][torch.argmax(classes)]})")


print(f"AB expected {exp_ab.size()}: {exp_ab}")

print(f"Raw AB result {ab.size()}: {ab}")

l = grayscale2l(shrinked_gray_image_tensor[0])
a = ab[0][0].detach()
b = ab[0][1].detach()

res_ab = torch.stack([torch.cat((a.unsqueeze(0), b.unsqueeze(0)), dim=0)])

lab_image = torch.cat((l.unsqueeze(0), a.unsqueeze(0), b.unsqueeze(0)), dim=0)

# Convert LAB image to RGB
rgb_result = LAB2RGBConversion()(lab_image)

# Display Lightness channel (L) - "grayscale"
ax[0].imshow(l, cmap='gray')
ax[0].set_title('L')

# Display A channel
ax[1].imshow(a, cmap='RdGy_r')
ax[1].set_title('A (Green - Red)')

# Display B channel
ax[2].imshow(b, cmap='RdBu_r')
ax[2].set_title('B (Blue - Yellow)')


# Display whole RGB  after conversion
ax[3].imshow(torchvision.transforms.ToPILImage()(rgb_result))
ax[3].set_title('Result')



print("Reconstr image:")
print(f"Total error: {torch.nn.functional.mse_loss(reconstr_image, shrinked_rgb_input_tensor, reduction='sum')}")
print(f"Colorfullness (avg saturation): {colorfullness(l, exp_a, exp_b)}")

print("Summary:")
print(f"A error (against reconstr): {torch.nn.functional.mse_loss(exp_a, a, reduction='sum')}")
print(f"B error (against reconstr): {torch.nn.functional.mse_loss(exp_b, b, reduction='sum')}")
print(f"Total error: {torch.nn.functional.mse_loss(rgb_result, shrinked_rgb_input_tensor, reduction='sum')}")
print(f"Colorfullness (avg saturation): {colorfullness(l, a, b)}")

fig.tight_layout()
fig.savefig(f"{os.path.splitext(os.path.basename(pth_path))[0]}-{os.path.splitext(os.path.basename(sys.argv[2]))[0]}-result.png", bbox_inches='tight')

plt.show()
