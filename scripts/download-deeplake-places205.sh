#!/bin/bash
#PBS -N dataset
#PBS -l select=1:ncpus=16:mem=16gb:scratch_local=20gb
#PBS -l walltime=10:00:00
#PBS -m ae

INDIR=/storage/brno12-cerit/home/xdvora3o/project-image-colorizer # IMPORTANT: Change path to the project location
OUTDIR=/storage/brno12-cerit/home/xdvora3o/project-image-colorizer # IMPORTANT: Change path to the project location
BATCH=download-deeplake-places205.sh
RUN_SCRIPT_PATH=scripts/singularity-download-deeplake-places205.py
STATUS=$OUTDIR/status.txt

export DEEPLAKE_DOWNLOAD_PATH="/storage/brno2/home/xnevrk03/data_knn"

export TMPDIR=$SCRATCHDIR
export RUN_SCRIPT_PATH=$RUN_SCRIPT_PATH


echo "$PBS_JOBID is running on node `hostname -f` in a scratch directory $SCRATCHDIR" >> $STATUS


# Scratch directory = temporary outputs
# test if scratch directory is set
test -n "$SCRATCHDIR" || { echo >&2 "Variable SCRATCHDIR is not set!"; exit 1; }
cd $SCRATCHDIR


cp -r $INDIR/* $SCRATCHDIR || { echo >&2 "Error while copying input file(s)!"; exit 2; }


singularity run --nv /cvmfs/singularity.metacentrum.cz/NGC/PyTorch\:23.11-py3.SIF ./scripts/singularity-job.sh

#python3 $SCRIPT>output || { echo >&2 "Calculation ended up erroneously (with a code $?) !!"; exit 3; }

# move the output to user's DATADIR or exit in case of failure
cp -r . $OUTDIR/ || { echo >&2 "Result file(s) copying failed (with a code $?) !!"; exit 4; }
echo "Output copied to $OUTDIR" >> $STATUS

# clean the SCRATCH directory
clean_scratch

